# School Assignments
This repository contains various coding & related assignments from my days at Kennesaw State University.

## CSE1321 (Python)
This class was a disaster from start to finish and I'm very thankful that I had previous Python experience to help me.

## CES3153 (Database Design & SQL)
An introductory course to databases, yet somehow still managed to mystify me at times. It probably didn't help that I had some prior experience with SQL, which I had to work around to actually learn the content.

## IT1113 (Python)
My first programming class in 5+ years. It was very informative and gave me a good foundation to continue off of.

## IT1323 (Java)
This was technically the second programming class in my "lower major requirements." I would have much preferred to continue with Python, but it isn't offered beyond the first of the two.

## IT3203 (HTML, CSS, JS)
Pretty much a walk in the park compared to all the rest, as I've been doing web design/development as a hobby for going on a decade now.

## IT4233 (Networking)
Some networking related exercises, most work was done on GDrive.

## Misc
Everything else and the kitchen sink!
