# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	9

# Defines the main function.
def main():
    
    # Ask user to input an integer value
    number = int(input('Enter number: '))
    
    # Prints the output and calls the sumDigits function.
    print('\nYou entered: ', number)
    print('Sum of digits: ', sumDigits(number))

# Defines the sumDigits function.
def sumDigits(number):
    
    # Initialize variable to count digits
    sumDigits = 0
    
    # Loop structure to count digits.
    while (number > 0):
        digit = number % 10
        
        sumDigits = sumDigits + digit
        
        number = number // 10
    
    # Returns the sum of the digits to the function call.
    return sumDigits

# Calls the main function.
main()