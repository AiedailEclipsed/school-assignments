# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 9

# Was not quite able to finish this program.

# Imports the random library
import random

# Defines the main function.
def main():
    
    # Creates a sentinel variable to loop program.
    keepGoing = 'y'
    
    # Creates loop structure.
    while (keepGoing == 'y'):
        
        # Calls the genRanNum function to generate a random number.
        secretNumber = genRanNum()
        
        # Calls the firstGuess function to take the users first guess.
        guess = firstGuess()
        
        # Calls the evaluate function to evaluate the users guess.
        print(evaluate(secretNumber, guess))
        
        # Asks the user if they would like to play again.
        keepGoing = input('\nWould you like to play again? (enter y to continue): ')
        keepGoing = keepGoing.lower()

# Defines the genRanNum function.
def genRanNum():
    
    # Sets the secret number as a random number between 1 & 20
    secretNumber = random.randint(1, 20)
    
    # Returns the secret number to the function call.
    return secretNumber

# Defines the firstGuess function.
def firstGuess():
    
    # Takes the user's first guess.
    guess = int(input('Guess the number between 1 and 20! What is your guess? '))
    
    # Returns the guess to the function call.
    return guess

# Defines the evaluate function.
def evaluate(secretNumber, guess):
    
    # Conditional tree to determine if the guess is correct.
    if (secretNumber == guess):
        return 'Correct!'
    elif (secretNumber > guess):
        return 'Too low!'
    elif (secretNumber < guess):
        return 'Too high!'

# Calls the main function.
main()