# Class:    CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor:   Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 9

# Was not quite able to finish this program.

# Defines the main function.
def main():
    
    startChar = validate(input('Enter starting character: '))
    endChar = validate(input('Enter ending character: ' ))
    
    print('Start character:', startChar)
    print('End character:', endChar)

# Defines the validate function.
def validate(char):
    allowedCharacters = 'abcdef'
    
    while(len(char) != 1 or char not in allowedCharacters):
        char = input('Invalid input. Please try again: ')
    
    return char

# Defines the printChars function.
def printChars(startChar, endChar):

main()