# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 13

# Imports the operator library.
import operator

# Defines the main function.
def main():
    
    # Creates base list & initialize counter variable.
    counter = 1
    array = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        array.append([])
        
        for index2 in range(4):
            print("Value #", counter, ": ", sep="", end="")
            value = int(input())
            
            array[index].append(value)
            counter += 1
    
    # Resets index.
    index = 0
    
    # Repetition strucuture to print array.
    print("\nThe entered matrix:")
    while (index < 3):
        print("", *array[index], sep="   ")
        index += 1
    
    # Resets index & calls the sumColumn function.
    index = 0
    sumArray = sumColumn(array)
    
    # Repetition structure to print the sum array.
    for index in range(4):
        if (index == 0):
            print("\nSum of column", index, "is", sumArray[index])
        else:
            print("Sum of column", index, "is", sumArray[index])

# Define the sumColumn function.
def sumColumn(array):
    
    # Creates the sum array.
    sumArray = [0] * 3
    
    # Adds the two arrays together.
    sumArray = list(map(operator.add, array[0], array[1]))
    sumArray = list(map(operator.add, sumArray, array[2]))
    
    # Returns the resultant array.
    return sumArray            

# Call the main function.
main()