# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 13

# Imports the operator library.
import operator

# Defines the main function.
def main():
    
    # Creates base lists & initialize counter variable.
    counter = 1
    array1 = []
    array2 = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        array1.append([])
        array2.append([])
        
        for index2 in range(3):
            print("Value #", counter, "A: ", sep="", end="")
            value = int(input())
            
            array1[index].append(value)
            
            print("Value #", counter, "B: ", sep="", end="")
            value = int(input())
            
            array2[index].append(value)
            counter += 1
    
    # Resets index.
    index = 0
    
    # Repetition strucuture to print array 1.
    print("\nMatrix A:")
    while (index < 3):
        print("", *array1[index], sep="   ")
        index += 1
    
    # Resets index.
    index = 0
    
    # Repetition structure to print array 2.
    print("\nMatrix B:")
    while (index < 3):
        print("", *array2[index], sep="   ")
        index += 1
    
    # Resets index & call the Addition function.
    index = 0
    addArray = Addition(array1, array2)
    
    # Repetition structure to print the addition array.
    print("\nA + B:")
    while (index < 3):
        print("", *addArray[index], sep="   ")
        index += 1

# Defines the Addition function.
def Addition(array1, array2):
    
    # Create base list.
    addArray = []
    
    # Repetition structure to create sublists and add 2 component lists together.
    for index in range(3):
        addArray.append([])
        
        for index2 in range(3):
            addArray[index] = list(map(operator.add, array1[index], array2[index]))
    
    # Returns the resultant array to function call.
    return addArray

# Calls the main function.
main()