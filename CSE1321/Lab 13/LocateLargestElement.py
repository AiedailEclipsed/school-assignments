# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 13

# Defines the main function.
def main():
    
    # Creates base list & initialize counter variable.
    counter = 1
    array = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        array.append([])
        
        for index2 in range(4):
            print("Value #", counter, ": ", sep="", end="")
            value = int(input())
            
            array[index].append(value)
            counter += 1
    
    # Resets index.
    index = 0
    
    # Repetition strucuture to print array.
    print("\nThe entered matrix:")
    while (index < 3):
        print("", *array[index], sep="   ")
        index += 1
    
    # Call the locateLargest function & print location.
    locationArray = locateLargest(array)
    print("\nFirst largest value is located at row", locationArray[0], "and column", locationArray[1])

# Defines the locateLargest function.
def locateLargest(array):
    
    # Finds the max value in each subarray.
    value0 = max(array[0])
    value1 = max(array[1])
    value2 = max(array[2])
    
    # Compares the values to find the overall max value & sets the location to an array.
    if (value0 >= value1 and value0 >= value2):
        locationArray = [0, array[0].index(max(array[0]))]
    elif (value1 >= value2):
        locationArray = [1, array[1].index(max(array[1]))]
    else:
        locationArray = [2, array[2].index(max(array[2]))]
    
    # Returns results to function call.
    return locationArray

# Calls the main function.
main()