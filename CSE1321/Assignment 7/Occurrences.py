# Class:    CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 7

# Defines the main function.
def main():
    
    # Creates a list of 10 values
    array = [0] * 10

    # Create the index & repetition structure to enter list values.
    index = 0

    while (index < len(array)):
        print("Value #", index + 1, ": ", sep="", end="")
        array[index] = int(input())
        
        index += 1
    
    # Prints output given the list and findIndex function returned value.
    print("\nEntered integers:", *array)
    Count(array)

# Defines the count function.
def Count(array):
    
    # Creates empty list to hold unique list items.
    countList = []
    
    # Create the index & repetition structure to get list of unique values.
    index = 0
    
    for index in array:
        if (index not in countList):
            countList.append(index)
    
    # Reset the index & create new repetition structure to print count of unique items.
    index = 0

    while (index < len(countList)):
        counter = array.count(countList[index])
        print(countList[index], "occurred", counter, "times")
        index += 1

# Calls the main function.
main()