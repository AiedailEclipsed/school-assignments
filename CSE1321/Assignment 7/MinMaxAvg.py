# Class:    CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 7

# Imports the random library for use.
import random

# Defines the main function.
def main():
    
    # Creates base list.
    grades = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(4):
        grades.append([])
        
        for index2 in range(4):
            grades[index].append(random.randint(0, 100))
    
    # Resets index.
    index = 0
    
    # Repetition strucuture to print grades list.
    print("Array Grades:")
    while (index < 4):
        print("", *grades[index], sep="   ")
        index += 1
    
    # Calls the minMaxAvg function.
    minMaxAvg(grades)
    
# Defines the minMaxAvg function.
def minMaxAvg(grades):
    
    # Calculates the highest grade, lowest grade, and class average based off list.
    highGrade = max([max(grades[0]), max(grades[1]), max(grades[2]), max(grades[3])])
    lowGrade = min([min(grades[0]), min(grades[1]), min(grades[2]), min(grades[3])])
    avg = ((sum(grades[0]) + sum(grades[1]) + sum(grades[2]) + sum(grades[3])) / 16)
    
    # Prints values found above.
    print("\nHighest grade:", highGrade)
    print("Lowest grade:", lowGrade)
    print("Class average:", format(avg, ".2f"))

# Calls the main function.
main()