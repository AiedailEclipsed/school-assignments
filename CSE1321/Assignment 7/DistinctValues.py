# Class:    CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 7

# Defines the main function.
def main():
    
    # Creates a list of 10 values
    array = [0] * 10

    # Create the index & repetition structure to enter list values.
    index = 0

    while (index < len(array)):
        print("Value #", index + 1, ": ", sep="", end="")
        array[index] = int(input())
        
        index += 1
    
    # Prints output given the list and getValues function returned list.
    print("\nOriginal array:", *array)
    print("Distinct array:", *getValues(array))

# Defines the getValues function.
def getValues(array):
    
    # Creates empty list to hold unique list items.
    distinctArray = []
    
    # Create the repetition structure to enter list values.
    for index in array:
        if (index not in distinctArray):
            distinctArray.append(index)
    
    # Returns the new list to function call.
    return distinctArray

# Calls the main function.
main()