# Class:    CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 7

# NOTE: Formatting might vary depending on where program is executed. Optimized based on personal IDE performance.

# Imports the random library for use.
import random

# Defines the main function.
def main():
    
    # Creates base list.
    hours = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        hours.append([])
        
        for index2 in range(7):
            hours[index].append(random.randint(0, 10))
    
    # Resets index.
    index = 0
    
    # Prints data based on lists.
    print("Employees Data: \n")
    print("\t", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", sep="\t")
    while (index < 3):
        print("Employee ", index + 1, sep="", end="\t\t")
        print(*hours[index], sep="\t")
        index += 1
    
    # Calls the addHours function.
    addHours(hours)

# Defines the addHours function.
def addHours(hours):
    
    # Prints the table header.
    print("\nEmployee # \t Weekly Hours")
    print("-------------------------")
    
    # Repetition structure to print sum of values in sublists.
    index = 0
    
    while (index < 3):
        print("    ", index + 1, "\t\t", sum(hours[index]))
        index += 1

# Calls the main function.
main()