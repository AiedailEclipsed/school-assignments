# Class:    CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 7

# Defines the main function.
def main():
    
    # Creates a list of 10 values
    array = [0] * 10

    # Create the index & repetition structure to enter list values.
    index = 0

    while (index < len(array)):
        print("Value #", index + 1, ": ", sep="", end="")
        array[index] = int(input())
        
        index += 1
    
    # Prints output given the list and findIndex function returned value.
    print("\nEntered integers:", *array)
    print("Index of largest value is", findIndex(array))

# Defines the findIndex function
def findIndex(array):
    
    # Finds the index of the highest value in the list & returns it.
    index = array.index(max(array))
    
    return index

# Calls the main function.
main()