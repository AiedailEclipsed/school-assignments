# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	5

# Asks user for coordinate input.
xValue = int(input('Please enter a X coordinate: '))
yValue = int(input('Please enter a Y coordinate: '))

# Prints out the coordinates as-is.
print('\nX-coordinate is', xValue)
print('Y-coordinate is', yValue)

# Conditional logic tree to determine where on a cartesian plane the point for the given coordinates is.
if (xValue == 0):
    if (yValue == 0):
        print('(', xValue,',', yValue,') is the origin point.', sep='')
    else:
        print('(', xValue,',', yValue,') is on the y-axis.', sep='')
elif (yValue == 0):
    print('(', xValue,',', yValue,') is on the x-axis.', sep='')
elif (xValue >= 1):
    if (yValue >= 1):
        print('(', xValue,',', yValue,') is in the first quadrant.', sep='')
    elif (yValue <= -1):
        print('(', xValue,',', yValue,') is in the fourth quadrant.', sep='')
elif (xValue <= -1):
    if (yValue >= 1):
        print('(', xValue,',', yValue,') is in the second quadrant.', sep='')
    elif (yValue <= -1):
        print('(', xValue,',', yValue,') is in the third quadrant.', sep='')