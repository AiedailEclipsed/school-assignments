# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	5

# Asks users for input based on current day and days till next meeting
today = int(input('Please enter today as a numerical value (starting with 0 for Sunday): '))
nextMeet = int(input('How many days till the next meeting? '))

# Conditional structure to output day of week
if (today == 0):
    print('\nToday is Sunday.')
elif (today == 1):
    print('\nToday is Monday.')
elif (today == 2):
    print('\nToday is Tuesday.')
elif (today == 3):
    print('\nToday is Wednesday.')
elif (today == 4):
    print('\nToday is Thursday.')
elif (today == 5):
    print('\nToday is Friday.')
elif (today == 6):
    print('\nToday is Saturday.')

# Prints the full current number of days till next meeting
print('Days to the next meeting is', nextMeet, 'days.')

# Conditional to cover if user inputs a negative number for nextMeet (can't have negative days) and to get a usable number if over 7
if (nextMeet < 0):
    print('Error. Please try again.')
elif (nextMeet >= 7):
    nextMeet = nextMeet % 7

# Conditional tree to output the day of the week based on nextMeet & current day
if (today == 0):
    if (nextMeet == 1):
        print('Meeting day is Monday.')
    elif (nextMeet == 2):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 3):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 4):
        print('Meeting day is Thursday.')
    elif (nextMeet == 5):
        print('Meeting day is Friday.')
    elif (nextMeet == 6):
        print('Meeting day is Saturday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Sunday.')
elif (today == 1):
    if (nextMeet == 1):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 2):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 3):
        print('Meeting day is Thursday.')
    elif (nextMeet == 4):
        print('Meeting day is Friday.')
    elif (nextMeet == 5):
        print('Meeting day is Saturday.')
    elif (nextMeet == 6):
        print('Meeting day is Sunday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Monday.')
elif (today == 2):
    if (nextMeet == 1):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 2):
        print('Meeting day is Thursday.')
    elif (nextMeet == 3):
        print('Meeting day is Friday.')
    elif (nextMeet == 4):
        print('Meeting day is Saturday.')
    elif (nextMeet == 5):
        print('Meeting day is Sunday.')
    elif (nextMeet == 6):
        print('Meeting day is Monday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Tuesday.')
elif (today == 3):
    if (nextMeet == 1):
        print('Meeting day is Thursday.')
    elif (nextMeet == 2):
        print('Meeting day is Friday.')
    elif (nextMeet == 3):
        print('Meeting day is Saturday.')
    elif (nextMeet == 4):
        print('Meeting day is Sunday.')
    elif (nextMeet == 5):
        print('Meeting day is Monday.')
    elif (nextMeet == 6):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Wednesday.')
elif (today == 4):
    if (nextMeet == 1):
        print('Meeting day is Friday.')
    elif (nextMeet == 2):
        print('Meeting day is Saturday.')
    elif (nextMeet == 3):
        print('Meeting day is Sunday.')
    elif (nextMeet == 4):
        print('Meeting day is Monday.')
    elif (nextMeet == 5):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 6):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Thursday.')
elif (today == 5):
    if (nextMeet == 1):
        print('Meeting day is Saturday.')
    elif (nextMeet == 2):
        print('Meeting day is Sunday.')
    elif (nextMeet == 3):
        print('Meeting day is Monday.')
    elif (nextMeet == 4):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 5):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 6):
        print('Meeting day is Thursday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Friday.')
elif (today == 6):
    if (nextMeet == 1):
        print('Meeting day is Sunday.')
    elif (nextMeet == 2):
        print('Meeting day is Monday.')
    elif (nextMeet == 3):
        print('Meeting day is Tuesday.')
    elif (nextMeet == 4):
        print('Meeting day is Wednesday.')
    elif (nextMeet == 5):
        print('Meeting day is Thursday.')
    elif (nextMeet == 6):
        print('Meeting day is Friday.')
    elif (nextMeet == 7 or nextMeet == 0):
        print('Meeting day is Saturday.')