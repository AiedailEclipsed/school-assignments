# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	5

# Asks user for input of four separate grades
grade1 = int(input('Please enter grade #1: '))
grade2 = int(input('Please enter grade #2: '))
grade3 = int(input('Please enter grade #3: '))
grade4 = int(input('Please enter grade #4: '))

# Prints out the grades in comma separated list
print('\nYou entered: ', grade1, ', ', grade2, ', ', grade3, ', ', grade4, sep='')

# Checks if all grades are same, edge case scenario
if (grade1 == grade2 and grade1 == grade3 and grade1 == grade4):
    highGrade = grade1
    lowGrade = grade1

# Conditional tree to find the highest grade
if (grade1 > grade2):
    if (grade1 > grade3):
        if (grade1 > grade4):
            highGrade = grade1
        else:
            highGrade = grade4
    else:
        highGrade = grade3
elif (grade2 > grade1):
    if (grade2 > grade3):
        if (grade2 > grade4):
            highGrade = grade2
        else:
            highGrade = grade4
    else:
        highGrade = grade3
elif (grade3 > grade1):
    if (grade3 > grade2):
        if (grade3 > grade4):
            highGrade = grade3
        else: highGrade = grade4
    else:
        highGrade = grade3
elif (grade4 > grade1):
    if (grade4 > grade2):
        if (grade4 > grade3):
            highGrade = grade4

# Conditional tree to find the lowest grade
if (grade1 < grade2):
    if (grade1 < grade3):
        if (grade1 < grade4):
            lowGrade = grade1
        else:
            lowGrade = grade4
    else:
        lowGrade = grade3
elif (grade2 < grade1):
    if (grade2 < grade3):
        if (grade2 < grade4):
            lowGrade = grade2
        else:
            lowGrade = grade4
    else:
        lowGrade = grade3
elif (grade3 < grade1):
    if (grade3 < grade2):
        if (grade3 < grade4):
            lowGrade = grade3
        else: lowGrade = grade4
    else:
        lowGrade = grade3
elif (grade4 < grade1):
    if (grade4 < grade2):
        if (grade4 < grade3):
            lowGrade = grade4

# Prints the highest and lowest grade
print('Highest grade:', highGrade)
print('Lowest grade:', lowGrade)

# Math operation to calculate the average grade
avgGrade = (grade1 + grade2 + grade3 + grade4) / 4

# Prints & formats the average grade
print('Average grade:', format(avgGrade, '.2f'))