# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	4

# User is prompted to enter their age & value is stored to age
age = int(input('Please enter your age: '))

# Beginning output with showing what user entered.
print('\nYou entered:', age)

# Conditional statement to display specific statement
if (age <= 21):
    print('Youth is a wonderful thing. Enjoy.')

# Ending output with always displaying this statement
print('Age is a state of mind.')