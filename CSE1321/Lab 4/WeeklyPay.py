# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	4

# User is prompted to enter their hours worked & value is stored to hours
hours = int(input('Please enter the number of hours you worked: '))

# Initialiaze earning variable.
earning = 0

# Beginning output with showing what user entered.
print('\nYou entered', hours, 'hours.')

# Conditional tree with logic based on amount of hours worked
if (hours > 40):
    earning = 40 * 10
    
    hours = hours - 40
    
    earning += hours * 15
else:
    earning = hours * 10

# Ending output to display gross earnings, based on conditional above
print('Gross earning is $', earning, sep = '')