# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	4

# User is prompted to enter their test grade and store it to grade.
grade = int(input('Please enter your test grade: '))

# Beginning output with showing what user entered.
print('\nYou entered:', grade)

# Conditional tree with logic to determine what statement to output based on grade value
if (grade >= 100):
    print('That grade is a perfect score. Well done.')
elif (grade >= 90 and grade <= 99):
    print('That grade is well above average. Excellent work.')
elif (grade >= 80 and grade <= 89):
    print('That grade is above average. Nice job.')
elif (grade >= 70 and grade <= 79):
    print('That grade is average work.')
elif (grade >= 60 and grade <= 69):
    print('That grade is not good, you should seek help!')
elif (grade < 60):
    print('That grade is not passing.')