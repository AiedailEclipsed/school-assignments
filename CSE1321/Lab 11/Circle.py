# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 11

# Did not have time to finish program.

import math

class Circle:
    def __init__(self, radius = 1):
        self.__radius = radius
        
    def getRadius(self):
        return self.__radius
    
    def getArea(self):
        area = math.pi * math.pow(self.__radius, 2)
        
        return area
                
    def getPerimeter(self):
        perimeter = 2 * math.pi * self.__radius
        
        return perimeter
        
    def toString(self):
        

circle = Circle()