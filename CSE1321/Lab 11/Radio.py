# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 11

# Import the random library for randomization.
import random

# Defines the Radio class.
class Radio:
    
    # Constructor with default values.
    def __init__(self):
        self.__station = 1
        self.__volume = 1
        self.__on = False
    
    # Defines the getStation method, returns the current station.
    def getStation(self):
        return self.__station
    
    # Defines the getVolume method, returns the current volume.
    def getVolume(self):
        return self.__volume
    
    # Defines the turnOn method, turns the radio on.
    def turnOn(self):
        self.__on = True
    
    # Defines the turnOff method, turns the radio off.
    def turnOff(self):
        self.__on = False
    
    # Defines the stationUp method, turns the station up by 1 if able.
    def stationUp(self):
        if (self.__on == True and self.__station >= 1 and self.__station <= 9):
            self.__station += 1
    
    # Defines the stationDown method, turns the station down by 1 if able.
    def stationDown(self):
        if (self.__on == True and self.__station >= 2 and self.__station <= 10):
            self.__station -= 1
    
    # Defines the volumeUp method, turns the volume up by 1 if able.
    def volumeUp(self):
        if (self.__on == True and self.__volume >= 1 and self.__volume <= 9):
            self.__volume += 1
    
    # Defines the volumeDown method, turns the volume down by 1 if able.
    def volumeDown(self):
        if (self.__on == True and self.__volume >= 2 and self.__volume <= 10):
            self.__volume -= 1
    
    # Defines the toString method, prints out messages.
    def toString(self):
        if (self.__on == True):
            print("\tThe radio station is ", self.__station, " and the volume level is ", self.__volume, ".", sep='')
        elif (self.__on == False):
            print("\tThe radio is off.")

# Creates a Radio class object.
radio = Radio()

# Initializes some varaibles for use.
rand = random.randint(1,8)
counter = 1
testOn = True
testVolUp = True
testStatUp = True
testVolDown = True
testStatDown = True
testOff = True
testOffVolUp = True
testOffStatDown = True

# Repetition loop to perform randomized tests.
while (counter <= 8):
    if (rand == 1 and testOn == True):
        print("Turn radio on:")
        radio.turnOn()
        radio.toString()
        testOn = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 2 and testVolUp == True):
        print("Turn volume up by 3:")
        radio.turnOn()
        radio.volumeUp()
        radio.volumeUp()
        radio.volumeUp()
        radio.toString()
        testVolUp = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 3 and testStatUp == True):
        print("Move station up by 5:")
        radio.turnOn()
        radio.stationUp()
        radio.stationUp()
        radio.stationUp()
        radio.stationUp()
        radio.stationUp()
        radio.toString()
        testStatUp = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 4 and testVolDown == True):
        print("Turn volume down by 1:")
        radio.turnOn()
        radio.volumeDown()
        radio.toString()
        testVolDown = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 5 and testStatDown == True):
        print("Turn station down by 3:")
        radio.turnOn()
        radio.stationDown()
        radio.stationDown()
        radio.stationDown()
        radio.toString()
        testStatDown = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 6 and testOff == True):
        print("Turn radio off:")
        radio.turnOff()
        radio.toString()
        testOff = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 7 and testOffVolUp == True):
        print("Turn volume up by 2:")
        radio.turnOff()
        radio.volumeUp()
        radio.volumeUp()
        radio.toString()
        testOffVolUp = False
        rand = random.randint(1,8)
        counter += 1
    elif (rand == 8 and testOffStatDown == True):
        print("Turn station down by 2:")
        radio.turnOff()
        radio.stationDown()
        radio.stationDown()
        radio.toString()
        testOffStatDown = False
        rand = random.randint(1,8)
        counter += 1
    else:
        rand = random.randint(1,8)