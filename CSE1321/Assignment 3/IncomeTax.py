# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 3

# Asks user for input of annual income
annualIncome = int(input('Please enter your annual income, rounded to the nearest dollar: $'))

# Prints the annual income
print('\nAnnual Income: $', annualIncome, sep='')

# Initialize the taxDue variable
taxDue = 0

# Conditional tree to print the tax bracket, and math operations to print the amount of tax due.
if (annualIncome <= 0):
    print('Invalid input. Please try again.')
elif (annualIncome <= 50000):
    print('Tax Bracket: 5%')
    taxDue = annualIncome * .05
    print('Tax due amount: $', format(taxDue, '.0f'), sep='')
elif (annualIncome > 50000 and annualIncome <= 200000):
    print('Tax Bracket: 10%')
    annualIncome = annualIncome - 50000
    taxDue = (annualIncome * .1) + 2500
    print('Tax due amount: $', format(taxDue, '.0f'), sep='')
elif (annualIncome > 200000 and annualIncome <= 400000):
    print('Tax Bracket: 15%')
    annualIncome = annualIncome - 200000
    taxDue = (annualIncome * .15) + 17500
    print('Tax due amount: $', format(taxDue, '.0f'), sep='')
elif (annualIncome > 400000 and annualIncome <= 900000):
    print('Tax Bracket: 25%')
    annualIncome = annualIncome - 400000
    taxDue = (annualIncome * .25) + 47500
    print('Tax due amount: $', format(taxDue, '.0f'), sep='')
elif (annualIncome > 900000):
    print('Tax Bracket: 35%')
    annualIncome = annualIncome - 900000
    taxDue = (annualIncome * .35) + 172500
    print('Tax due amount: $', format(taxDue, '.0f'), sep='')