# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 3

# Asks user for input of box values
smallWeight = int(input('Please enter the weight of the small box (in pounds): '))
smallPrice = int(input('Please enter the price of the small box: $'))
largeWeight = int(input('Please enter the weight of the large box (in pounds): '))
largePrice = int(input('Please enter the price of the large box: $'))

# Outputs the values entered above with formatting
print('\nSmall box weight:', smallWeight, 'Pounds')
print('Small box price:', smallPrice, 'Dollars')
print('Large box weight:', largeWeight, 'Pounds')
print('Large box price:', largePrice, 'Dollars')

# Determines the value of each box
smallValue = smallPrice / smallWeight
largeValue = largePrice / largeWeight

# Conditional tree to compare box values, then display proper message.
if (smallValue == largeValue):
    print('Judgement: Both boxes are of the same value.')
elif (smallValue > largeValue):
    print('Judgement: The large box is a better deal.')
elif (largeValue > smallValue):
    print('Judgement: The smaller box is a better deal.')