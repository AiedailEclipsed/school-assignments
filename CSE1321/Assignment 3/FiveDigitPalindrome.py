# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 3

# Asks user to input a five digit number
number = int(input('Please enter a five digit number: '))

# Initialize variable to store reversed number and create counter for loop
reverseNum = 0
counter = number

# Conditional tree to determine if it is within range
if (number >= 11111 and number <= 99999):
    
    # Loop to reverse the number
    while (counter != 0):
        reverseNum = (reverseNum * 10) + (counter % 10)
        counter = counter // 10
    
    # Conditional tree to evaluate if the number is a palindrome
    if (number == reverseNum):
       print('Valid 5-digit palindrome.')
    else:
        print('Invalid 5-digit palindrome.')
else:
    print('Invalid 5-digit number. Try again.')