# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 3

# Asks user to input account number and account type
accountNum = int(input('Please enter your account number: '))
serviceType = input('What is your account type (R for regular, P for Premium): ')

# Conditional tree to determine how to print and math operations based on account type
if (serviceType == 'r' or serviceType == 'R'):
    
    # Asks user to input minutes
    totalMinutes = int(input('How many minutes are you using? '))
    
    # Sets the base price paid no matter what
    amountDue = 15
    
    # Adds additional per minute cost over 50 minutes
    if (totalMinutes >= 51):
        amountDue += (totalMinutes - 50) * .5
    
    # Prints results.
    print('\nAccount Number:', accountNum)
    print('Service type: Regular')
    print('Total minutes:', totalMinutes)
    print('Amount due: $', format(amountDue, '.2f'), sep='')
elif (serviceType == 'p' or serviceType == 'P'):
    
    # Asks user to input day and nighttime minutes
    dayMinutes = int(input('How many minutes are you using between 6AM and 6PM? '))
    nightMinutes = int(input('How many minutes are you using between 6PM and 6AM? '))
    
    # Sets the base price paid no matter what
    amountDue = 25
    
    # Addtional per minute cost depending on time of day
    if (dayMinutes >= 51):
        amountDue += (dayMinutes - 50) * .2
    if (nightMinutes >= 101):
        amountDue += (nightMinutes - 100) * .1 
    
    # Prints results
    print('\nAccount Number:', accountNum)
    print('Service type: Premium')
    print('Daytime minutes:', dayMinutes)
    print('Nighttime minutes:', nightMinutes)
    print('Amount due: $', format(amountDue, '.2f'), sep='')
else:
    
    # Gives error message if user doesn't enter correct value for account type
    print('\nInvalid input. Please try again.')