# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 3

# Asks user to input coordinates & radius of 2 circles
valueX1 = int(input('X coordinate of circle #1: '))
valueY1 = int(input('Y coordinate of circle #1: '))
radius1 = int(input('Radius of circle #1: '))
valueX2 = int(input('X coordinate of circle #2: '))
valueY2 = int(input('Y coordinate of circle #2: '))
radius2 = int(input('Radius of circle #2: '))

print('\nCircle 1 center is: (', valueX1, ',', valueY1, ')', sep='')
print('Circle 1 radius is: ', radius1)
print('Circle 2 center is: (', valueX2, ',', valueY2, ')', sep='')
print('Circle 2 radius is: ', radius2)

# Find the distance between the two points
distance = ((valueX2 - valueX1)**2 + (valueY2 - valueY1)**2)**.5

sumRadius = radius1 + radius2

# Note: I was unable to figure out the logic behind how to determine whether they were overlapping, touching, etc.