# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 5

# Defines the main function.
def main():
    
    # Prints the table header.
    print('Feet', 'Meter', '\t', 'Meter', 'Feet', sep='\t')
    
    # Sets the counter as floating point 1
    counter = float(1)
    
    # Loop structure to print out conversions using function calls.
    while (counter <= 20):
        print(counter, feetToMeter(counter), '\t', counter, meterToFeet(counter), sep='\t')
        
        # Iterates the counter variable
        counter = counter + 1

# Defines the feetToMeter function
def feetToMeter(counter):
    
    # Converts feet to meters.
    meter = counter * .305
    
    # Returns the meters formatted to the 3rd decimal place.
    return format(meter, '.3f')

# Defines the meterToFeet function.
def meterToFeet(counter):
    
    # Converts the meter to feet.
    feet = counter * 3.279
    
    # Returns the feet formatted to the 3rd decimal place.
    return format(feet, '.3f')

# Calls the main function.
main()