# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 5

# Did not have time to fully finish program.

# Defines the main function.
def main():
    counter = 11
    
    for temp in range(5):
        for temp2 in range(10):
            if (isPalindrome(counter) == True):
				print(counter, end=' ')
                counter = counter + 1
            else:
                counter = counter + 1
        print('\r')

# Defines the isPalindrome function.
def isPalindrome(number):
    
    # Initialize variable to store reversed number and create counter for loop
    reverseNum = 0
    counter = number
    
    # Loop to reverse the number
    while (counter != 0):
        reverseNum = (reverseNum * 10) + (counter % 10)
        counter = counter // 10
    
    # Conditional tree to evaluate if the number is a palindrome
    if (number == reverseNum):
		return True
    else:
		return False

# Defines the isPrime function.
def isPrime(number):
    
    # Loop to check if number is a prime.
    for temp in range(2, number):
        if ((number % temp) != 0):
            return True
        else:
            return False
            break

# Calls the main function.
main()