# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 5

# Defines the main function.
def main():
    
    # Asks for user input of an integer value
    limit = int(input('Please enter an integer value: '))
    
    # Calls the displaySums function.
    displaySums(limit)

# Defines the displaySums function.
def displaySums(limit):
    
    # Initializes counter & sum variable.
    counter = 1
    sum = 0
    
    # Prints table header.
    print('\ni','Sum(i)', sep='\t')
    
    # Loop structure to print table values.
    while (counter <= limit):
        
        # Finds the new sum.
        sum = (counter / (counter + 1)) + sum
        
        # Prints current table row.
        print(counter, format(sum, '.4f'), sep='\t')
        
        # Interates counter variable.
        counter = counter + 1

# Calls the main function.
main()