# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 5

# Define the main function.
def main():
    
    # Asks for user input of rectangle width and height.
    width = int(input('What is the rectangle\'s width? '))
    height = int(input('What is the rectangle\'s height? '))
    
    # Prints out the user input width and height.
    print('\nEntered width:', width)
    print('Entered height:', height)
    
    # Conditional tree to using isValid function to determine which message to print.
    if (isValid(width, height) == True):
        print('Area:', Area(width, height))
        print('Perimeter:', Perimeter(width, height))
    elif (isValid(width, height) == False):
        print('This is an invalid rectangle. Try again.')

# Defines the isValid function.
def isValid(width, height):
    
    # Finds the sum of height and width.
    sum = width + height
    
    # Conditional tree to return true/false based on sum
    if (sum > 30):
        return True
    else:
        return False

# Defines the Area function
def Area(width, height):
    
    # Finds the area of the rectangle
    area = width * height
    
    # Returns the area to the function call.
    return area

# Defines the Perimeter function
def Perimeter(width, height):
    
    # Finds the perimeter of the rectangle
    perimeter = (width * 2) + (height * 2)
    
    # Returns the perimeter to the function call.
    return perimeter

# Calls the main function.
main()