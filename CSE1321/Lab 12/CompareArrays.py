# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 12

# Defines the main function.
def main():
    
    # Asks user to define the size of the arrays
    arraySize = int(input("What is the size of the arrays? "))
    
    # Creates the lists based on the user input.
    array1 = [0] * arraySize
    array2 = [0] * arraySize
    
    # Create the index & repetition structure to enter values.
    index = 0
    
    while (index < len(array1)):
        print("Array 1, value #", index + 1, ": ", sep="", end="")
        array1[index] = int(input())
        
        index += 1
    
    index = 0
    
    while (index < len(array2)):
        print("Array 2, value #", index + 1, ": ", sep="", end="")
        array2[index] = int(input())
        
        index += 1
    
    # Prints basic output, given array size and both arrays.
    print("\nArray size:", arraySize)
    print("First array:", end=" ")
    print(*array1, sep=", ")
    print("Second array:", end=" ")
    print(*array2, sep=", ")
    
    # Conditional structure to determine what to print based on Compare function.
    if (Compare(array1, array2) == True):
        print("Judgement: The arrays are identical.")
    elif (Compare(array1, array2) == False):
        print("Judgement: The arrays are not identical.")

# Defines the Compare function.
def Compare(array1, array2):
    
    # Conditional structure to determine if lists are identical.
    if (array1 == array2):
        return True
    elif (array1 != array2):
        return False        

# Calls the main function.
main()