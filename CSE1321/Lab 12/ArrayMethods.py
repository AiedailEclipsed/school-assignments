# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 12

# Import the random & math library for use.
import random, math

# Defines the main function.
def main():
    
    # Creates a 5 item list.
    array = [0] * 5
    
    # Create the index & repetition structure to populate list randomly.
    index = 0

    while (index < len(array)):
        array[index] = random.randint(1, 100)
        index += 1
    
    # Prints output using function calls.
    print("Original array:", end=" ")
    print(*array, sep=", ")
    print("Max value:", arrayMax(array))
    print("Min value:", arrayMin(array))
    arraySquared(array)
    print("Squared array:", end=" ")
    print(*array, sep=", ")
    arrayReverse(array)
    print("Reversed array:", end=" ")
    print(*array, sep=", ")

# Defines the arrayMax function.
def arrayMax(array):
    
    # Finds & returns the max value of the list.
    maxValue = max(array)
    
    return maxValue

# Defines the arrayMin function.
def arrayMin(array):
    
    # Finds & returns the min value of the list.
    minValue = min(array)
    
    return minValue

# Defines the arraySquared function.
def arraySquared(array):
    
    # Create the index & repetition structure to set list values to their squared value.
    index = 0
    
    while (index < len(array)):
        array[index] = int(math.pow(array[index], 2))
        index += 1

# Defines the arrayReverse function.
def arrayReverse(array):
    
    # Reverses the array.
    array.reverse()

# Calls the main function.
main()
    