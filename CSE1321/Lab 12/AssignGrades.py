# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 12

# Defines the main function.
def main():
    
    # Asks user for size of class.
    classSize = int(input("What is the size of your class? "))
    
    # Creates a list based on class size.
    grades = [0] * classSize

    # Create the index & repetition structure to enter grade values.
    index = 0

    while (index < len(grades)):
        print("Student #", index + 1, " score: ", sep="", end="")
        grades[index] = int(input())
        
        while (grades[index] < 0 or grades[index] > 100):
            grades[index] = int(input("Invalid input. Please input score again: "))
        
        index += 1
    
    # Prints basic output, given class size and grade list.
    print("\nClass size:", classSize)
    print("Entered grades:", end=" ")
    print(*grades, sep=", ")
    print("")
    
    # Calls the printGrades function to print dynamic output.
    printGrades(grades)

# Define the printGrades function, with the grades list as argument.
def printGrades(grades):
    
    # Create the index & repetition structure to print verbose student scores.
    index = 0
    
    while (index < len(grades)):
        if (grades[index] <= 100 and grades[index] >= 90):
            print("Student", index + 1, "score is", grades[index], "and grade is A.")
        elif (grades[index] <= 89 and grades[index] >= 80):
            print("Student", index + 1, "score is", grades[index], "and grade is B.")
        elif (grades[index] <= 79 and grades[index] >= 70):
            print("Student", index + 1, "score is", grades[index], "and grade is C.")
        elif (grades[index] <= 69 and grades[index] >= 60):
            print("Student", index + 1, "score is", grades[index], "and grade is D.")
        elif (grades[index] < 60):
            print("Student", index + 1, "score is", grades[index], "and grade is F.")
        
        index += 1

# Calls the main function.
main()