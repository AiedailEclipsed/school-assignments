# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	3

# Ask user to input values for variables X, Y, Z
valueX = int(input('Please enter the value of integer X: '))
valueY = int(input('Please enter the value of integer Y: '))
valueZ = int(input('Please enter the value of integer Z: '))

# Find the sum of all 3 variables
sum = valueX + valueY + valueZ

# Use the sum, then divide by 3 for average
average = sum / 3

# Print values and average.
print('X = ', valueX)
print('Y = ', valueY)
print('Z = ', valueZ)
print('Average = ', average)