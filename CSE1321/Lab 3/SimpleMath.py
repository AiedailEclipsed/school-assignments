# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	3

# Ask user to input values for variables R & S
valueR = float(input('Please enter the value of floating-point number R: '))
valueS = float(input('Please enter the value of floating-point number S: '))

# Find the sum, difference, and product of variable R & S
sum = valueR + valueS
difference = valueR - valueS
product = valueR * valueS

# Print values and all found operations.
print('R = ', valueR)
print('S = ', valueS)
print('R + S = ', sum)
print('R - S = ', difference)
print('R * S = ', product)