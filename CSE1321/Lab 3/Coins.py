# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	3

# Ask user to input number of quarters, dimes, nickels, & pennies
quarters = int(input('Please enter the number of quarters: '))
dimes = int(input('Please enter the number of dimes: '))
nickels = int(input('Please enter the number of nickels: '))
pennies = int(input('Please enter the number of pennies: '))

# Find various values in order to find the final total in dollars & cents.
totalQuarters = quarters * 25
totalDimes = dimes * 10
totalNickels = nickels * 5

totalCents = totalQuarters + totalDimes + totalNickels + pennies

totalDollars = totalCents // 100

totalCents = totalCents % 100

# Print values and total.
print('\nYou entered', quarters, 'quarters.')
print('You entered', dimes, 'dimes.')
print('You entered', nickels, 'nickels.')
print('You entered', pennies, 'pennies.')
print('\nYour total is', totalDollars, 'dollars and', totalCents, 'cents.')