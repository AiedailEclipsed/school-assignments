# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	2

print('My Name: Christopher Ryan Smith')
print('My Birthday: April 4, 1994')
print('My Hobbies: Reading, playing video games, and writing.')
print('My Favorite Book: Murder on the Orient Express by Agatha Christie')
print('My Favorite Movie: Avatar')