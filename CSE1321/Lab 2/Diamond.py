# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	2

print("               *")
print("              * *")
print("             * * *")
print("            * * * *")
print("             * * *")
print("              * *")
print("               *")