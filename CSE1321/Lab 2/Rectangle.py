# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	2

width = 4
height = 8

area = width * height
perimeter = (width + height) * 2

print('The width = ', width)
print('The height = ', height)
print('The area = ', area)
print('The perimeter = ', perimeter)