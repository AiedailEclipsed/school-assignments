# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	8

# Defines the main function.
def main():
    
    # Asks user to input an integer.
    number = int(input('Please enter an integer: '))
    
    # Prints the input number and uses a conditonal to determine what judgement to print.
    print('\nEntered value: ', number)
    if (isPalindrome(number) == True):
        print('Judgement: Palindrome')
    elif (isPalindrome(number) == False):
        print('Judgement: Not palindrome')

# Defines the function to reverse the number
def reverse(number):
    
    # Initializes variable to store reversed number and create counter for loop
    reverseNum = 0
    counter = number
    
    # Loop to reverse the number
    while (counter != 0):
        reverseNum = (reverseNum * 10) + (counter % 10)
        counter = counter // 10
    
    # Returns the reversed number to the function call.
    return reverseNum

# Defines the function to determine if the number is a palindrome.
def isPalindrome(number):
    
    # Calls the reverse function.
    reverseNum = reverse(number)
    
    # Conditional statement to determine if the reversed number is a palindrome. Returns true/false to function call.
    if (reverseNum == number):
        return True
    else:
        return False

# Calls the main function.
main()