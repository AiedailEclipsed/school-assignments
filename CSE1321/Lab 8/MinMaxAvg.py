# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	8

# Define the main function.
def main():
    
    # Asks for user input of 3 integers.
    value1 = int(input('Please enter an integer: '))
    value2 = int(input('Please enter an integer: '))
    value3 = int(input('Please enter an integer: '))
    
    # Displays the results of the input and calls various functions to calculate values.
    print('\nYou entered: ', value1, ', ', value2, ', ', value3, sep='')
    print('Max value:', max(value1, value2, value3))
    print('Min value:', min(value1, value2, value3))
    print('Average value:', format(average(value1, value2, value3), '.0f'))

# Define the maximum value function.
def max(value1, value2, value3):
    
    # Conditional tree to determine maximum value.
    if (value1 >= value2 and value1 >= value3):
        max = value1
    elif (value2 >= value3):
        max = value2
    else:
        max = value3
    
    # Returns the maximum value to function call.
    return max

# Define the minimum value function.
def min(value1, value2, value3):
    
    # Conditional tree to determine minimum value.
    if (value1 <= value2 and value1 <= value3):
        min = value1
    elif (value2 <= value3):
        min = value2
    else:
        min = value3
    
    # Returns the minimum value to function call.
    return min

# Define the average value function.
def average(value1, value2, value3):
    
    # Calculates the average.
    average = (value1 + value2 + value3) / 3
    
    # Returns the average value to the function call.
    return average

# Calls the main function.
main()