# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	8

# Imports the math module
import math

# Defines the main function.
def main():
    
    # Asks for user input of various values.
    sqSide = float(input('What is the length of one side of the square? '))
    rectWidth = float(input('What is the width of the rectangle? '))
    rectLength = float(input('What is the length of the rectangle? '))
    circRadius = float(input('What is the radius of the circle? '))
    triBase = float(input('What is the length of the base of the triangle? '))
    triHeight = float(input('What is the height of the triangle? '))
    
    # Outputs the input values and calls functions to calculate other values.
    print('\nSquare side: ', sqSide)
    print('Square area: ', format(squareArea(sqSide), '.2f'))
    print('\nRectangle width: ', rectWidth)
    print('Rectangle length: ', rectLength)
    print('Rectangle area: ', format(rectangleArea(rectWidth, rectLength), '.1f'))
    print('\nCircle radius: ', circRadius)
    print('Circle area: ', format(circleArea(circRadius), '.3f'))
    print('\nTriangle base: ', triBase)
    print('Triangle height: ', triHeight)
    print('Triangle area: ', format(triangleArea(triBase, triHeight), '.2f'))

# Defines the area of a square function.
def squareArea(sqSide):
    
    # Calculates the area of a square.
    area = math.pow(sqSide, 2)
    
    # Returns the area to the function call.
    return area

# Defines the area of a rectangle function.
def rectangleArea(rectWidth, rectLength):
    
    # Calculates the area of a rectangle
    area = rectWidth * rectLength
    
    # Returns the area to the function call.
    return area

# Defines the area of a circle function.
def circleArea(circRadius):
    
    # Calculates the area of a circle.
    area = math.pi * (math.pow(circRadius, 2))
    
    # Returns the area to the function call.
    return area

# Defines the area of a triangle function.
def triangleArea(triBase, triHeight):
    
    # Calculates the area of a triangle
    area = (triHeight * triBase) / 2
    
    # Returns the area to the function call.
    return area

# Calls the main function.
main()