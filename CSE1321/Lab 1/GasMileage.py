# Program GasMileage.py
# Calculates fuel efficiency based on values entered by the user.    

miles = int(input ("Enter the number of miles: ") )
gallons = int(input("Enter the gallons of fuel used: ") )

mpg = miles / gallons 
print ("Miles Per Gallon: ", mpg)
