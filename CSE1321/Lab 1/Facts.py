# Program Facts.py
# Demonstrate string concatenation operator conversion of an integer to a string. 
 
# Prints various facts. 
    
# Strings can be concatenated into one long string 
print ("We present the following facts for your ", " extracurricular edification:")        
      
# A string can contain numeric digits 
print ("Letters in the Hawaiian alphabet: 12")       
      
# A numeric value can be concatenated to a string 
print ("Dialing code for Antarctica: ", 672) 
print ("Year in which Leonardo da Vinci invented ", "the parachute: ", 1515) 
print ("Speed of ketchup: ", 40 ," km per year")
