# Program CountDown.py
# Demonstrate how to print to console/screen. 

# Prints two lines of output representing a rocket countdown. 
    
print ("Three...", end =" ")# end = “ “ prevents print from creating a new line
print ("Two...", end =" ")  # end = “ “ prevents print from creating a new line
print ("One...", end =" ")  # end = “ “ prevents print from creating a new line
print ("Zero...", end =" ") # end = “ “ prevents print from creating a new line
print ("Liftoff!") 
print ("Houston, we have a problem.")
