# Class: CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor: Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 8

# Defines the main function.
def main():
    
    # Creates base lists & initialize counter variable.
    counter = 1
    array1 = []
    array2 = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        array1.append([])
        array2.append([])
        
        for index2 in range(3):
            print("Value #", counter, "A: ", sep="", end="")
            value = int(input())
            
            array1[index].append(value)
            
            print("Value #", counter, "B: ", sep="", end="")
            value = int(input())
            
            array2[index].append(value)
            counter += 1
    
    # Resets index.
    index = 0
    
    # Repetition strucuture to print array 1.
    print("\nArray A:")
    while (index < 3):
        print("", *array1[index], sep="   ")
        index += 1
    
    # Resets index.
    index = 0
    
    # Repetition structure to print array 2.
    print("\nArray B:")
    while (index < 3):
        print("", *array2[index], sep="   ")
        index += 1
    
    # Conditional structure to determine judgement based on function isEquivalent.
    if (isEquivalent(array1, array2) == True):
        print("\nJudgement: The arrays are equivalent.")
    elif (isEquivalent(array1, array2) == False):
        print("\nJudgement: The arrays are not equivalent.")

# Defines the isEquivalent function.
def isEquivalent(array1, array2):
    
    # Create blank lists to hold flattened lists.
    flatArray1 = []
    flatArray2 = []
    
    # Repetition structures to flatten lists.
    for index in array1:
        for index2 in index:
            flatArray1.append(index2)
    
    for index in array2:
        for index2 in index:
            flatArray2.append(index2)
    
    # Sort flattened lists.
    flatArray1.sort()
    flatArray2.sort()
    
    # Conditional structure to compare lists and return results.
    if (flatArray1 == flatArray2):
        return True
    else:
        return False

# Calls the main function.
main()