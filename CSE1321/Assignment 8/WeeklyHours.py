# Class: CSE 1321
# Section: W01        
# Term: Spring 2019  
# Instructor:   Douglas Malcolm
# Name: C. Ryan Smith   
# Assignment #: 8

# NOTE: Formatting might vary depending on where program is executed. Optimized based on personal IDE performance.

# Imports the random library for use.
import random

# Defines the main function.
def main():
    
    # Creates base list.
    hours = []
    
    # Repetition structure to create sublists and populate them.
    for index in range(3):
        hours.append([])
        
        for index2 in range(7):
            hours[index].append(random.randint(0, 10))
    
    # Resets index.
    index = 0
    
    # Prints data based on lists.
    print("Employees Data: \n")
    print("\t", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", sep="\t")
    while (index < 3):
        print("Employee ", index + 1, sep="", end="\t")
        print(*hours[index], sep="\t")
        index += 1
    
    # Resets index and adds space.
    index = 0
    print("")
    
    # Print data of pulled from mostHours function.
    while (index < 3):
        print("Employee ", index + 1, "worked most hours on", mostHours(hours[index]))
        index += 1
    
    # Calls the addHours function.
    addHours(hours)

# Defines the mostHours function.
def mostHours(hours):
    
    # Finds the index of the highest value in list.
    day = hours.index(max(hours))
    
    # Conditional structure to return day of week based on highest hours.
    if (day == 0):
        return "Monday"
    elif (day == 1):
        return "Tuesday"
    elif (day == 2):
        return "Wednesday"
    elif (day == 3):
        return "Thursday"
    elif (day == 4):
        return "Friday"
    elif (day == 5):
        return "Saturday"
    elif (day == 6):
        return "Sunday"

# Defines the addHours function.
def addHours(hours):
    
    # Create base list.
    totalHours = []
    
    # Repetition structure to hold sublist of employees & hours worked total.
    for index in range(3):
        totalHours.append([index + 1, sum(hours[index])])
    
    # Sort list based on number of hours worked per employee.
    totalHours.sort(key=lambda hoursWorked : hoursWorked[1])
    
    # Prints the table header.
    print("\nEmployee # \t Weekly Hours")
    print("-------------------------")
    
    # Repetition structure to print list.
    index = 0
    
    while (index < 3):
        print(" ", *totalHours[index], sep="\t")
        index += 1

# Calls the main function.
main()