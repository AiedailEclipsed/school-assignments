# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: EXTRA CREDIT

# Defines the Dab class.
class Dab:
    
    # Constructor with default values & prints the initial emote.
    def __init__(self):
        self._rightArm = 0
        self._leftArm = 0
        
        print("\n--(•_•)--")
    
    # Defines the DoTheDab method, which moves the position of the arms & prints the dabbing emote.
    def DoTheDab(self):
        self._leftArm += 120
        
        print("\n--( •_)>")
        
# Defines the main function.
def main():
    
    # Creates sentinel variable.
    sentinel = None
    
    # Repetition loop to repeat program.
    while (sentinel != "quit"):
        
        # Creates object of Dab class.
        dab = Dab()
        
        # Asks user if they want to dab, converts input to lowercase.
        sentinel = input("\nDo the dab (enter \"yes\" to continue)? ")
        sentinel = sentinel.lower()
        
        # Input validation to make sure they said yes, will repeat until they do.
        while (sentinel != "yes"):
            sentinel = input("Invalid input. Please enter \"yes\" to continue: ")
            sentinel = sentinel.lower()
        
        # Calls Dab method DoTheDab on dab object.
        dab.DoTheDab()
        
        # Sentinel value checkpoint
        sentinel = input("\nDo you want to stop (enter \"quit\" at any time to exit)? ")

# Calls the main function.
main()