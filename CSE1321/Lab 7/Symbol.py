# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	7

# Asks for user input.
symbol = input('Please enter a symbol: ')
number = int(input('Please enter a number: '))

# Input validation to make sure number is higher than 0
while (number <= 0):
    number = int(input('Invalid input. Please enter a number greater than 0: '))

# First loop, handles the number of rows. 
for temp in range(number):
    
    # Second loop, creates the shape itself.
    for temp in range(number):
        print(symbol, end='')
      
    # Prints onto new line at end of each line. 
    print('\r')