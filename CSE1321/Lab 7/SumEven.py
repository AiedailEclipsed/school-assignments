# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	7

# Asks user for number input.
num = int(input('Please enter a number between 20 and 60: '))

# Input validation to make sure number is between 20 and 60
while (num <= 19 or num >= 61):
    num = int(input('Invalid input. Please enter a number between 20 and 60. '))

# Initialize variables for use
counter = 2
sumEven = 0

# Loop to add even numbers to total
while (counter < (num + 1)):
    sumEven = sumEven + counter
    counter = counter + 2

# Outputs the input number and found total.
print('Entered value:', num)
print('Sum of even numbers between 2 and', num, 'is', sumEven)