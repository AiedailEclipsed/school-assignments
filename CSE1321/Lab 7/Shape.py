# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	7

# Initialize counter variable
shape = 8
  
# First loop, handles the number of rows. 
for temp in range(shape): 
      
    # Second loop, centers the shape.
    for temp2 in range(shape): 
        print(end=' ') 
      
    # Decrements the shape counter
    shape = shape - 1
      
    # Third loop, creates the shape itself.
    for temp2 in range(temp+1): 
        if (temp2 == 0):
            print('*', end='')
        else:
            print('**', end='') 
      
    # Prints onto new line at end of each line. 
    print('\r')