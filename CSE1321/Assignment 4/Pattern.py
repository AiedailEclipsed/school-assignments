# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 4

# NOTE: Couldn't quite figure out how to flip the shape.

for temp in range(6, 0, -1):
    num = 1
    
    for temp2 in range(0, temp):
        print(num, end=' ')   
        num = num + 1
    
    print('\r')