# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 4

# Asks user to input an integer
number = int(input('Enter positive integers (0 to quit): '))

# Initializes variables for use
largestValue = number
occur = 0

# Loop to continue input until sentinel value is entered
while (number != 0):
    # Conditional to ignore negative numbers
    if (number >= 1):
        # Counts the occurences of the largest value
        if (number == largestValue):
            occur = occur + 1
        # Checks to see if current number is larger than current largest value
        if (number > largestValue):
            largestValue = number
            occur = 1
    
    number = int(input('Enter positive integers (0 to quit): '))

# Outputs the largest value and number of occurences
print('Largest value:', largestValue)
print('Occurrences:', occur, 'times')