# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 4

# NOTE: Couldn't quite get the inverted part of the shape to work.

# Initialize counter variable
shape = 4

# First primary loop
for temp in range(shape, 0, -1):
    for temp2 in range(temp):
        print('**', end='')
    
    for temp2 in range(shape):
        print(end=' ')
    
    shape = shape + 1
    
    print('\r')

# Initialize counter variable
shape = 5

# Second primary loop, handles the number of rows. 
for temp in range(shape): 
      
    # First loop, centers the shape.
    for temp2 in range(shape): 
        print(end=' ') 
      
    # Decrements the shape counter
    shape = shape - 1
      
    # Second loop, creates the shape itself.
    for temp2 in range(temp+1): 
        if (temp2 == 0):
            print('*', end='')
        else:
            print('**', end='') 
      
    # Prints onto new line at end of each line. 
    print('\r')