# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 4

# Create sentinel variable
keepGoing = 'y'

# Loop to continue program until sentinel value is changed
while (keepGoing == 'y'):
    # Asks user for height in inches
    height = int(input('What is your height in inches? '))
    
    # Input validation to make sure height is at least 1 inch
    while (height <= 0):
        height = int(input('Invalid height. Please enter a height of at least 1 inch. '))
    
    # Asks user for weight in pounds
    weight = int(input('What is your weight in pounds? '))

    # Input validation to make sure weight is at least 1 pound
    while (weight <= 0):
        weight = int(input('Invalid weight. Please enter a weight of at least 1 pound. '))

    # Asks user for age in years
    age = int(input('What is your age in years? '))

    # Input validation to make sure age is at least 1 year
    while (age <= 0):
        age = int(input('Invalid age. Please enter an age of at least 1 year. '))

    # Asks user for gender, converts input to uppercase
    gender = input('What is your gender (use M for male and F for female)? ')
    gender = gender.upper()
    
    # Input validation to make sure gender is in correct format, converts to uppercase
    while (gender != 'F' and gender != 'M'):
        gender = input('Invalid input. Please use M for male and F for female: ')
        gender = gender.upper()
    
    # Conditional tree to find BMR based on gender
    if (gender == 'F'):
        BMR = 655 + (4.35 * weight) + (4.7 * height) - (4.7 * age)
    elif (gender == 'M'):
        BMR = 66 + (6.23 * weight) + (12.7 * height) - (6.8 * age)
    
    # Outputs exercise level options and asks user to input their own
    print('Please select your level of exercise from the options below.')
    print('1. You don\'t exercise.')
    print('2. You engage in light exercise one to three days a week.')
    print('3. You exercise moderately three to five times a week.')
    print('4. You exercise intensely six to seven days a week.')
    print('5. You exercise intensely six to seven days a week and have a physically active job.')
    exerciseLevel = int(input('Exercise Level #'))
    
    # Input validation to make sure exercise is between 1 and 5
    while (exerciseLevel <= 0 or exerciseLevel >= 6):
        exerciseLevel = int(input('Invalid input. Please enter 1-5 depending on your exercise level above: '))
    
    # Conditional tree to find DCA based on exercise level
    if (exerciseLevel == 1):
        DCA = BMR * 1.2
    elif (exerciseLevel == 2):
        DCA = BMR * 1.375
    elif (exerciseLevel == 3):
        DCA = BMR * 1.55
    elif (exerciseLevel == 4):
        DCA = BMR * 1.725
    elif (exerciseLevel == 5):
        DCA = BMR * 1.9
    
    # Conditional tree to display appropriate data
    if (gender == 'F'):
        print('Female, ', height, '", ', weight, ' lbs, age ', age, ', BMR = ', format(BMR, '.1f'), ', Exercise ', exerciseLevel, ', DCA: ', format(DCA, '.2f'), sep='')
    elif (gender == 'M'):
        print('Male, ', height, '", ', weight, ' lbs, age ', age, ', BMR = ', format(BMR, '.1f'), ', Exercise ', exerciseLevel, ', DCA: ', format(DCA, '.2f'), sep='')
    
    # Sentinel value checkpoint
    keepGoing = input('Do you want to run the program again (enter y to continue)? ')