# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	6

# Import the random library to generate a random number
import random

# For loop to repeat until we have 10 numbers
for temp in range(10):
    # Print a random number between 1 and 50
    print(random.randint(1, 50))