# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	6

# Asks user for input of a number between 1 and 100
number = int(input('Please enter a number between 1 and 100: '))

# Prints out the input
print('\nYou entered:', number)

# Conditional tree to determine if input is valid
if (number <= 0 or number >= 101):
    print('Invalid Input. Try again.')
elif (number >= 1 and number <= 100):
    # Create a counter variable for loop and variable to hold the sum
    counter = 1
    sum = 1
    
    # Loop to determine the sum of all values between 1 and entered number
    while (counter != number):
        counter = counter + 1
        sum = sum + counter
    
    # Print out the sum of values
    print('Sum of values:', sum)