# Class:	CSE 1321L
# Section: W03         
# Term:	Spring 2019  
# Instructor:	Rehnuma Afrin
# Name:	C. Ryan Smith   
# Lab#:	6

# Create a counter variable
counter = 50

# Print out the beginning statement
print('Even numbers between 50 and 100: ', end='')

# Loop to print up to 100 limit
while (counter < 101):
    # Conditional tree to make sure it ends without a trailing comma
    if (counter == 100):
        print(counter)
        counter = counter + 1
    else:    
        print(counter, end=', ')
        counter = counter + 2

# Reset the counter to the odd equivalent
counter = 51

# Print out the beginning statement
print('\nOdd numbers between 50 and 100: ', end='')

# Loop to print up to the 100 limit
while (counter < 100):
    # Conditional tree to make sure it ends without a trailing comma
    if (counter == 99):
        print(counter)
        counter = counter + 1
    else:    
        print(counter, end=', ')
        counter = counter + 2