# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 6

# Imports the random library for randomization.
import random

# Defines the Counter class.
class Counter:
    
    # Constructor with default values.
    def __init__(self):
        self.__value = 0
    
    # Defines the increment method, increments value by 1.
    def increment(self):
        self.__value += 1
    
    # Defines the getValue method, returns the current value.
    def getValue(self):
        return self.__value

# Creates two Counter objects.
heads = Counter()
tails = Counter()

# Initializes some variables.
rand = 0
counter = 1

# Repetition structure to flip a coin 100 times and record their accumulated value.
while (counter <= 100):
    rand = random.choice(["Heads", "Tails"])
    print(rand)
    if (rand == "Heads"):
        heads.increment()
    elif (rand == "Tails"):
        tails.increment()
    counter += 1

# Prints the final total of each coin toss.
print("Total Heads: ", heads.getValue())
print("Total Tails: ", tails.getValue())