# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 6

# Imports the math module for advanced calculations.
import math

# Defines the QuadraticEquation class.
class QuadraticEquation:
    
    # Constructor with default values and user defined values.
    def __init__(self, a = 1, b = 1, c = 1):
        self.__a = a
        self.__b = b
        self.__c = c
    
    # Defines the getA method, returns the A value.
    def getA(self):
        return self.__a
    
    # Defines the getB method, returns the B value.
    def getB(self):
        return self.__b
    
    # Defines the getC method, returns the C value.
    def getC(self):
        return self.__c
    
    # Defines the getDiscriminant method, calculates and returns the discriminant value.
    def getDiscriminant(self):
        discValue = math.pow(self.__b, 2) - (4 * self.__a * self.__c)
        
        return discValue
    
    # Defines the getRoot1 method, calculates and returns the value.
    def getRoot1(self):
        root1 = (-self.__b + math.sqrt(self.getDiscriminant())) / (2 * self.__a)
        
        return root1        
    
    # Defines the getRoot2 method, calculates and returns the value.
    def getRoot2(self):
        root2 = (-self.__b - math.sqrt(self.getDiscriminant())) / (2 * self.__a)
        
        return root2

# Create sentinel variable/
keepGoing = "y"

# Repetition structure to repeat program.
while (keepGoing == "y"):
    
    # Prompts user for input.
    aValue = int(input("A Value: "))
    bValue = int(input("B Value: "))
    cValue = int(input("C Value: "))
    
    # Creates QuadraticEquation object based on user input.
    quad = QuadraticEquation(aValue, bValue, cValue)
    
    # Prints values.
    print("\na = ", quad.getA())
    print("b = ", quad.getB())
    print("c = ", quad.getC())
    
    # Conditional structure to determine if root values will be printed.
    if (quad.getDiscriminant() < 0):
        print("Root 1 is Undefined.")
        print("Root 2 is Undefined.")
    else:
        print("Root 1 = ", quad.getRoot1())
        print("Root 2 = ", quad.getRoot2())
    
    # Sentinel variable guard point.
    keepGoing = input("\nDo you want to run the program again (enter y to continue)? ")