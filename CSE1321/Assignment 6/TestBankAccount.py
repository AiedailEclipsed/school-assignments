# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 6

# Imports the datetime library for date related functionality.
import datetime

# Defines the BankAccount class.
class BankAccount:
    
    # Constructor with default values and user-defined values.
    def __init__(self, ID = 0, balance = 0.0, AIR = 0.0):
        self.__id = ID
        self.__balance = balance
        self.__annualInterestRate = AIR
        self.__dateCreated = datetime.datetime.now()
    
    # Defines the getID method, returns the current ID.
    def getID(self):
        return self.__id
    
    # Defines the getBalance method, returns the current balance.
    def getBalance(self):
        return self.__balance
    
    # Defines the getAIR method, returns the current AIR.
    def getAIR(self):
        return self.__annualInterestRate
    
    # Defines the getDate method, returns the time the object was created.
    def getDate(self):
        return self.__dateCreated.strftime("%c")
    
    # Defines the setID method, changes the ID based on input.
    def setID(self, ID):
        self.__id = ID
    
    # Defines the setBalance method, changes the balance based on input.
    def setBalance(self, balance):
        self.__balance = balance
    
    # Defines the setAIR method, changes the AIR based on input.
    def setAIR(self, AIR):
        self.__annualInterestRate = AIR
    
    # Defines the getMonthlyInterestRate method, returns the value.
    def getMonthlyInterestRate(self):
        MIR = self.getAIR() / 12
        
        return MIR
    
    # Defines the getMonthlyInterest method, returns the value.
    def getMonthlyInterest(self):
        interest = self.getBalance() * self.getMonthlyInterestRate()
        
        return interest
    
    # Defines the withdraw method, subtracts user input from balance.
    def withdraw(self, withdrawal):
        self.__balance -= withdrawal
    
    # Defines the deposit method, adds user input to balance.
    def deposit(self, deposit):
        self.__balance += deposit
    
    # Defines the toString method, prints certain values.
    def toString(self):
        print("Account ID:", self.__id)
        print("Account Balance: $", self.__balance, sep="")
        print("Interest Rate: ", self.__annualInterestRate, "%", sep="")
        print("Date Opened:", self.__dateCreated.strftime("%c"))

# Object test data.
myObject = BankAccount(123456, 10000, 2.5)
myObject.toString()
myObject.withdraw(3500)
myObject.deposit(500)
print("Account Balance: $", myObject.getBalance(), sep="")
print("Monthly Interest: $", format(myObject.getMonthlyInterest(), ".2f"), sep="")
print("Date Account Created:", myObject.getDate())