# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 14

# Import the random library for use.
import random

# Defines the main function.
def main():
    
    # Creates base list.
    sortArray = []
    
    # Repetition structure to randomly populate list.
    for index in range(50):
        sortArray.append(random.randint(0, 100))
    
    # Print array values.
    print('Array Values:', *sortArray)
    
    # Calls the various functions.
    BubbleSort(sortArray)
    InsertionSort(sortArray)
    SelectionSort(sortArray)

# Defines the BubbleSort function.
def BubbleSort(sortArray):
    
    # Initialize swaps variable to count swaps done.
    swaps = 0
    
    # Repetition & conditional structure to sort array & count swaps done.
    for index in range(len(sortArray)):
        for index2 in range(0, len(sortArray) - index - 1):
            if (sortArray[index2] > sortArray[index2 + 1]):
                temp = sortArray[index2]
                sortArray[index2] = sortArray[index2 + 1]
                sortArray[index2 + 1] = temp
                swaps += 1
    
    # Print output based on function operations.
    print('\nBubble Sorted values:', *sortArray)
    print('Bubble Sort Swaps:', swaps)

# Defines the InsertionSort function.
def InsertionSort(sortArray):
    
    # Initialize swaps variable to count swaps done.
    swaps = 0
    
    # Repetition & conditional structure to sort array & count swaps done.
    for index in range (1, len(sortArray)):
        currentValue = sortArray[index]
        position = index
        
        while (position > 0 and sortArray[position - 1] > currentValue):
            sortArray[position] = sortArray[position - 1]
            position = position - 1
        
        sortArray[position] = currentValue
        swaps += 1
    
    # Print output based on function operations.
    print('\nInsertion Sorted values:', *sortArray)
    print('Insertion Sort Swaps:', swaps)

# Defines the SelectionSort function.
def SelectionSort(sortArray):
    
    # Initialize swaps variable to count swaps done.
    swaps = 0
    
    # Repetition & conditional structure to sort array & count swaps done.
    for index in range(len(sortArray)):
        minimum = index
        
        for index2 in range(index + 1, len(sortArray)):
            if (sortArray[minimum] > sortArray[index2]):
                minimum = index2
        
        temp = sortArray[index]
        sortArray[index] = sortArray[minimum]
        sortArray[minimum] = temp
        swaps += 1
    
    # Print output based on function operations.
    print('\nSelection Sorted values:', *sortArray)
    print('Selection Sort Swaps:', swaps)

# Calls the main function.
main()