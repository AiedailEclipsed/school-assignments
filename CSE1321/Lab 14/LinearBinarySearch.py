# Class: CSE 1321L
# Section: W03         
# Term: Spring 2019  
# Instructor: Rehnuma Afrin
# Name: C. Ryan Smith   
# Lab#: 14

# Defines the main function.
def main():
    
    # Creates a list of 10 values
    searchArray = [0] * 10
    
    # Create the index & repetition structure to enter list values.
    index = 0
    
    while (index < len(searchArray)):
        print("Value #", index + 1, ": ", sep="", end="")
        searchArray[index] = int(input())
        
        index += 1
    
    # Prompt user for target value.
    targetValue = int(input('\nWhat value are you looking for? '))
    
    # Print array & target value.
    print('\nArray Values:', *searchArray)
    print('Target value:', targetValue)
    
    # Call functions to find comparison values.
    linearComparisons = LinearSearch(searchArray, targetValue)
    binaryComparisons = BinarySearch(searchArray, targetValue)
    
    # Conditional trees to figure out which statement to print.
    if (linearComparisons > len(searchArray)):
        print('\nTarget Value not Found in Linear Search!')
    else:
        print('\nLinear Search Comparisons:', linearComparisons)
    
    if (binaryComparisons > len(searchArray)):
        print('Target Value not Found in Binary Search!')
    else:
        print('Binary Search Comparisons:', binaryComparisons)

# Defines the LinearSearch function.
def LinearSearch(searchArray, targetValue):
    
    # Initialize variable.
    comparisons = 0
    
    # Repetition structure to find number of comparisons made till target found.
    for index in range (len(searchArray)):
        if (searchArray[index] == targetValue):
            comparisons += 1
            
            return comparisons
        else:
            comparisons += 1
    
    # Returns array size + 1 to signal value not found.
    return (len(searchArray) + 1)

# Defines the BinarySearch function.
def BinarySearch(searchArray, targetValue):
    
    # Initialize variables & sort list.
    comparisons = 0
    searchArray.sort()
    firstBound = 0
    lastBound = len(searchArray) - 1
    
    # Repetition structure to find number of comparisons made till target found.
    while (firstBound <= lastBound):
        midpoint = (firstBound + lastBound) // 2
        comparisons += 1
        
        if (searchArray[midpoint] == targetValue):
            return comparisons
        elif (targetValue < searchArray[midpoint]):
            lastBound = midpoint - 1
        else:
            firstBound = midpoint + 1
    
    # Returns array size + 1 to signal value not found.
    return (len(searchArray) + 1)

# Calls the main function.
main()