# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 2

# Ask user to input values for 2 points
valueX1 = float(input('Enter X1: '))
valueY1 = float(input('Enter Y1: '))
valueX2 = float(input('Enter X2: '))
valueY2 = float(input('Enter Y2: '))

# Find the distance between the two points
distance = ((valueX2 - valueX1)**2 + (valueY2 - valueY1)**2)**.5

# Print the distance between the points
print('Distance: ', distance)