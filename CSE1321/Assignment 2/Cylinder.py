# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 2

# Ask user to input values for variables radius & length
radius = float(input('Enter radius: '))
length = float(input('Enter length: '))

# Find the area and volume of the cylinder
area = radius * radius * 3.14
volume = area * length

# Print the volume, formatted to the 2nd decimal place
print('The volume is ', format(volume, '.2f'))