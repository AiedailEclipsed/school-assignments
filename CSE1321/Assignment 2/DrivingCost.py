# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 2

# Ask user to input values for distance traveled in miles, miles per gallon, and price per gallon in dollars.
distance = float(input('Distance traveled (miles): '))
mpg = float(input('Miles per gallon (miles): '))
price = float(input('Price per gallon (dollars): '))

# Calculate the tripCost in total.
tripCost = (distance / mpg) * price

# Print the trip cost in dollars, formatted to the 2nd decimal place.
print('Trip cost (dollars): ', format(tripCost, '.2f'))