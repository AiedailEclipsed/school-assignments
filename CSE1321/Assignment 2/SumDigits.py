# Class:	CSE 1321
# Section: W01        
# Term:	Spring 2019  
# Instructor:	Douglas Malcolm
# Name:	C. Ryan Smith   
# Assignment #: 2

# Ask user to input an integer value
number = int(input('Enter number: '))

# Initialize variable to count digits
sumDigits = 0

# Create conditional tree to count digits
# First step is to establish out of bounds numbers
if (number > 1000 or number < 0):
    print('That number is out of bounds. Please try again.')
# Second step is to account for edge case
elif (number == 1000):
    sumDigits = 1
    print('Sum of digits: ', sumDigits)
# Last step is actual logic to count digits
else:    
    sumDigits += (number // 100)
    
    number = number % 100
    
    sumDigits += (number // 10)
    
    number = number % 10
    
    sumDigits = sumDigits + number
    
    print('Sum of digits: ', sumDigits)