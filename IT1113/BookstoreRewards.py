# Program Name: BookstoreRewards.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #2
# Due Date: 09.16.2018

# Purpose: This program asks users to input their first name, last name, and the number of books that they have pruchased this month. Afterward, it displays this information along with the number of points they have earned for the month.

# Display store information & instructions.
print('Welcome to the Read-It-All Bookstore Book Club! \nPlease follow the onscreen instructions below to calculate the number of points you have earned this month. \n----------')

# Ask user for first and last name.
fullName = input('Please enter your first & last name: ')

# Ask user for number of books bought.
numBooks = int(input('How many books have you bought? '))

# Calculate the number of points awarded.
if numBooks == 1 or numBooks == 2:
    numPoints = 5
elif numBooks == 3 or numBooks == 4:
    numPoints = 10
elif numBooks == 5 or numBooks == 6:
    numPoints = 15
elif numBooks == 7 or numBooks == 8:
    numPoints = 20
elif numBooks >= 9:
    numPoints = 25
else:
    numPoints = 0

# Output the name, number of books bought, and points awarded for the month.
print('----------')
print('Name: ', fullName)
print('Books Bought: ', numBooks)
print('Points Earned: ', numPoints)

# Display goodbye message.
print('---------- \nHave a wonderful day!')