# Program Name: Olympics.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Exam #3
# Due Date: 12.9.2018

# Purpose: This program asks for input of five scores, then removes the highest and lowest, and averages the rest to return the final score.

# Display program instructions.
print('Olympic Event Score Calculator \nPlease enter each judges\' score below. \n----------')

# Define the main function.
def main():
    
    # Create the score entry list.
    Scores = [0] * 5
    
     # Create the index & repetition structure to enter score values.
    index = 0
    
    while index < len(Scores):
        print('Judge #', index + 1, ': ', sep='', end='')
        Scores[index] = float(input())
        index += 1
    
    # Calls the finalScore function to display the averaged score.
    print('----------')
    print('Final Score: ', format(finalScore(Scores), '.2f'), ' points', sep='')
    
    # Display goodbye message.
    print('----------\nHave a wonderful day!')

# Define the finalScore function.
def finalScore(Scores):
    
    # Removes the lowest & highest value from the Scores list.
    Scores.remove(min(Scores))
    Scores.remove(max(Scores))
    
    # Create accumulator variable.
    totalScore = 0
    
    # Repetition structure to calculate the total.
    for num in Scores:
        totalScore += num
    
    # Finds the average score using the totalScore and dividing it by the length of the list.
    averageScore = totalScore / len(Scores)
    
    # Returns the average score to be displayed.
    return averageScore

# Call main function.
main()