# Program Name: GolfOutput.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #5
# Due Date: 11.04.2018

# Purpose: Part 2 of the Lab #5. This program will read data from golf.txt and use it to display information.

# Display program instructions.
print('Marietta Country Club Golf Tournament Standings \n----------')

# Define the main function.
def main():
    
    # Added functionality for flexible par score.
    par = int(input('What is par for the course? '))
    print('----------')
    
    # Opens the golf.txt file from previous program, readable only.
    golfIn = open('golf.txt', 'r')
    
    # Reads the first name line.
    firstName = golfIn.readline()
    
    # Creates our heading structure.
    print('First Name', 'Last Name', 'Handicap', 'Score\t', 'Status', sep='\t')
    print('----------', '----------', '----------', '----------', '----------', sep='\t')
    
    # Creates a loop to read and display all data until the end of the file (firstName being empty).
    while firstName != '':
        
        # Reads the next few lines.
        lastName = golfIn.readline()
        handicap = golfIn.readline()
        score = golfIn.readline()
        
        # Strips the lines of new line characters.
        firstName = firstName.rstrip('\n')
        lastName = lastName.rstrip('\n')
        handicap = handicap.rstrip('\n')
        score = score.rstrip('\n')
        
        # Determine whether the score was over, under, or made par.
        if score > str(par):
            status = 'Over Par'
        elif score == str(par):
            status = 'Made Par'
        elif score < str(par):
            status = 'Under Par'
        else:
            print('Error!')
        
        # Prints and displays the information.
        print(firstName, lastName, handicap, score, status, sep='\t \t')
        
        # Reads the next line as the next first name.
        firstName = golfIn.readline()
    
    # Closes our reading file.
    golfIn.close()

# Calls the main function.
main()