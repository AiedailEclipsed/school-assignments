# Program Name: TuitionCalculator.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #3
# Due Date: 09.30.2018

# Purpose: This program asks the user to input their tuition per semester for 2017, then displays that along with the next 7 years of tuition per semester given an increase of 3.5% per year.

# Display program instructions.
print('7 Year Tuition Calculator\n----------')
tuition = float(input('Please enter your tuition per semester for 2017: $'))

# Creates the repetition structure using 2017 as the starting year.
year = 2018

for year in range(2018, 2025):
    
    # Creates the tuition display header.
    if year == 2018:
        print('----------\nYEAR\tTUITION')
        tuition = tuition * 1.035
        print(year, format(tuition, '.2f'), sep='\t')
    
    # Preforms the actual tuition calculations for future years and displays them.
    else:
        tuition = tuition * 1.035
        print(year, format(tuition, '.2f'), sep='\t')

