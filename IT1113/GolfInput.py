# Program Name: GolfInput.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #5
# Due Date: 11.04.2018

# Purpose: Part 1 of the Lab #5. This program will take input and convert it to a text file for processing in another program.

# Display program instructions.
print('Golf Tournament Data Entry \nPlease follow the instructions below. \n----------')

# Define the main function.
def main():
    
    # Create the repetition logic structure.
    numPlayers = int(input('How many players do you have to enter? '))
    print('----------')
    
    # Open a writeable text file for output.
    golfOut = open('golf.txt', 'w')
    
    # The actual repetition structure that we created earlier to take input.
    for count in range(1, numPlayers + 1):
        
        # Iterating prompt for data input.
        print('Player #', count, sep='')
        firstName = input('First Name: ')
        lastName = input('Last Name: ')
        handicap = float(input('Handicap: '))
        golfScore = int(input('Score: '))
        
        # Writes data to output file.
        golfOut.write(firstName + '\n')
        golfOut.write(lastName + '\n')
        golfOut.write(str(handicap) + '\n')
        golfOut.write(str(golfScore) + '\n')
        
        print('----------')
    
    # Close the output file.
    golfOut.close()
    
    # Display text file information & goodbye message.
    print('Player information written to golf.txt. \nHave a wonderful day!')

# Call the main function.
main()