# Program Name: Rainfall.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #6
# Due Date: 11.18.2018

# Purpose: This program takes the rainfall for each month, then displays the total rainfall, the average rainfall, and the months with the lowest and higher rainfall.

# Display program instructions.
print('Rainfall Statistics \nPlease enter the rainfall in inches for each month below. \n----------')

# Define the main function.
def main():
    
    # Create the rainfall entry list & month name tuple.
    months = ('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December')
    rainfall = [0] * len(months)
    
    # Create the index & repetition structure to enter rainfall values.
    index = 0
    
    while index < len(rainfall):
        print(months[index], ': ', sep='', end='')
        rainfall[index] = float(input())
        index += 1
    
    # Calls various functions to display information.
    print('----------')
    print('Total Rainfall: ', format(totalRainfall(rainfall), '.2f'), ' inches', sep='')
    print('Average Monthly Rainfall: ', format(averageRainfall(rainfall), '.2f'), ' inches', sep='')
    print('Month with Highest Rainfall: ', highestMonth(rainfall, months), ' (', max(rainfall), ' inches)', sep='')
    print('Month with Lowest Rainfall: ', lowestMonth(rainfall, months), ' (', min(rainfall), ' inches)', sep='')
    
    # Display goodbye message.
    print('----------\nHave a wonderful day!')

# Define the totalRainfall function.
def totalRainfall(rainfall):
    
    # Create accumulator variable.
    total = 0
    
    # Repetition structure to calculate the total.
    for num in rainfall:
        total += num
    
    # Returns the total rainfall to be displayed.
    return total

# Define the averageRainfall function.
def averageRainfall(rainfall):
    
    # Finds the average rainfall using the totalRainfall function and dividing it by the length of the list.
    average = totalRainfall(rainfall) / len(rainfall)
       
    # Returns the average rainfall to be displayed.
    return average

# Define the highestMonth function.
def highestMonth(rainfall, months):
      
    # Finds the month by using the index position of the highest rainfall.
    highMonth = months[rainfall.index(max(rainfall))]
    
    # Returns the highest rainfall month to be displayed.
    return highMonth

# Define the lowestMonth function.
def lowestMonth(rainfall, months):
    
    # Finds the corresponding month by using the index position of the lowest rainfall.
    lowMonth = months[rainfall.index(min(rainfall))]
    
    # Returns the lowest rainfall month to be displayed.
    return lowMonth

# Call main function.
main()