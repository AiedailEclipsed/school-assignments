# Program Name: ColorCalculator.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Exam #1
# Due Date: 09.23.2018

# Purpose: This program asks users to input two primary colors and will then output the resulting secondary color when the two are mixed. If anything other than red, blue, or yellow is input, an error message should result.

# Display program instructions.
print('Color Calculator \nPlease enter a primary color (red, blue, or yellow) when prompted, the program will then show the resulting secondary color. \n----------')

# Create a repetition structure which allows us to ask for more input.
keepGoing = 'y'

while keepGoing == 'y':
    
    # Asks for the first primary color.
    color1 = input('Please enter the first primary color: ')
    
    # Asks for the second primary color.
    color2 = input('Please enter the second primary color: ')
    
    # Nested decision structure used to compare and calculate the resulting secondary color.
    if color1 == 'red' or color1 =='Red':
        if color2 == 'blue' or color2 =='Blue':
            print('Red + Blue = Purple \n----------')
        elif color2 == 'yellow' or color2 =='Yellow':
            print('Red + Yellow = Orange \n----------')
        elif color2 == 'red' or color2 =='Red':
            print('You have entered the same colors. \n----------')
        else:
            print('Error! You have entered improper input. Please try again. \n----------')
    elif color1 == 'blue' or color1 =='Blue':
        if color2 == 'red' or color2 =='Red':
            print('Blue + Red = Purple \n----------')
        elif color2 == 'yellow' or color2 =='Yellow':
            print('Blue + Yellow = Green \n----------')
        elif color2 == 'blue' or color2 =='Blue':
            print('You have entered the same colors. \n----------')
        else:
            print('Error! You have entered improper input. Please try again. \n----------')
    elif color1 == 'yellow' or color1 =='Yellow':
        if color2 == 'red' or color2 =='Red':
            print('Yellow + Red = Orange \n----------')
        elif color2 == 'blue' or color2 =='Blue':
            print('Yellow + Blue = Green \n----------')
        elif color2 == 'yellow' or color2 =='Yellow':
            print('You have entered the same colors. \n----------')
        else:
            print('Error! You have entered improper input. Please try again. \n----------')
    else:
        print('Error! You have entered improper input. Please try again. \n----------')
    
    # Exit point for repetition structure.
    keepGoing = input('Do you want to calculate another secondary color (enter y to continue)? ')