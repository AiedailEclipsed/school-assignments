# Program Name: KilometerConverter.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Exam #2
# Due Date: 10.21.2018

# Purpose: This program asks for a distance in kilometers then converts that to miles. It uses a separate function for the conversion and uses a loop to elegantly continue or discontinue conversion.

# Display program instructions.
print('Kilometer to Mile Converter \nPlease enter a distance in kilometers. \n----------')

# Define the main function.
def main():
    
    # Create a repetition structure which allows us to ask for more input.
    keepGoing = 'y'
    
    while keepGoing == 'y':
        
        # Ask the user for distance in kilometers.
        kilometers = float(input('Kilometers: '))
        
        # Calls the conversion function and prints the output, while formatting the results to two decimal places.
        print('Miles: ', format(conversion(kilometers), '.2f'))
        print('----------')
        
        # Exit point for repetition structure.
        keepGoing = input('Do you want to convert another distance to miles (enter y to continue)? ')
        print('----------')

    # Display goodbye message.
    print('Have a wonderful day!')

# Defines the conversion function.
def conversion(num):
    
    # Converts the passed value to miles & then returns it.
    result = num * 0.6214
    
    return result

# Calls the main function.
main()