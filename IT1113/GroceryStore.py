# Program Name: GroceryStore.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #1
# Due Date: 08.26.2018

# Purpose: This program asks for the name and price for up to 8 items and then displays all items and prices in a nicely formatted list.

# Display store information & instructions.
print('Welcome to Imagination Station! \nPlease follow the onscreen instructions below. \n----------')

# Ask user for first item.
item1 = input('Please enter the name of the first item: ')
price1 = float(input('Please enter the price of the first item: $'))

# Ask user for second item.
item2 = input('Please enter the name of the second item: ')
price2 = float(input('Please enter the price of the second item: $'))

# Ask user for third item.
item3 = input('Please enter the name of the third item: ')
price3 = float(input('Please enter the price of the third item: $'))

# Ask user for fourth item.
item4 = input('Please enter the name of the fourth item: ')
price4 = float(input('Please enter the price of the fourth item: $'))

# Ask user for fifth item.
item5 = input('Please enter the name of the fifth item: ')
price5 = float(input('Please enter the price of the fifth item: $'))

# Ask user for sixth item.
item6 = input('Please enter the name of the sixth item: ')
price6 = float(input('Please enter the price of the sixth item: $'))

# Ask user for seventh item.
item7 = input('Please enter the name of the seventh item: ')
price7 = float(input('Please enter the price of the seventh item: $'))

# Ask user for eighth item.
item8 = input('Please enter the name of the eighth item: ')
price8 = float(input('Please enter the price of the eighth item: $'))

# Display item list.
print('----------')
print(item1, format(price1, '.2f'), sep='\t \t')
print(item2, format(price2, '.2f'), sep='\t \t')
print(item3, format(price3, '.2f'), sep='\t \t')
print(item4, format(price4, '.2f'), sep='\t \t')
print(item5, format(price5, '.2f'), sep='\t \t')
print(item6, format(price6, '.2f'), sep='\t \t')
print(item7, format(price7, '.2f'), sep='\t \t')
print(item8, format(price8, '.2f'), sep='\t \t')

# Display goodbye message.
print('---------- \nHave a wonderful day!')