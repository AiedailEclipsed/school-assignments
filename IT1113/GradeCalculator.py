# Program Name: GradeCalculator.py
# Course: IT1113, Section W03
# Student Name: C. Ryan Smith
# Assignment Number: Lab #4
# Due Date: 10.14.18

# Purpose: This program asks users to input a student name and eight test scores. It will then display the student's letter grade for each test along with the average test score.

# Display program instructions.
print('Average Grade Calculator \nPlease enter the student\'s name and test grades when prompted. \n----------')

# Define the main function.
def main():
    
    # Create a repetition structure which allows us to ask for more input.
    keepGoing = 'y'
    
    while keepGoing == 'y':
        
        # Ask user for student name and eight test scores.
        studentName = input('Student Name: ')
        testScore1 = int(input('Test Score #1: '))
        testScore2 = int(input('Test Score #2: '))
        testScore3 = int(input('Test Score #3: '))
        testScore4 = int(input('Test Score #4: '))
        testScore5 = int(input('Test Score #5: '))
        testScore6 = int(input('Test Score #6: '))
        testScore7 = int(input('Test Score #7: '))
        testScore8 = int(input('Test Score #8: '))
        
        # Output the student name & call the other functions to output letter grades and average scores.
        print('----------')
        print(studentName)
        determine_grade(testScore1)
        determine_grade(testScore2)
        determine_grade(testScore3)
        determine_grade(testScore4)
        determine_grade(testScore5)
        determine_grade(testScore6)
        determine_grade(testScore7)
        determine_grade(testScore8)
        calc_average(testScore1, testScore2, testScore3, testScore4, testScore5, testScore6, testScore7, testScore8)
        print('----------')
        
        # Exit point for repetition structure.
        keepGoing = input('Do you want to calculate another student\'s average grade (enter y to continue)? ')
    
    # Display goodbye message.
    print('---------- \nHave a wonderful day!')

# Define the required calc_average function.        
def calc_average(score1, score2, score3, score4, score5, score6, score7, score8):
    
    # Add all scores together and find average.
    sum = score1 + score2 + score3 + score4 + score5 + score6 + score7 + score8
    
    average = sum // 8
    
    # Uses average grade and finds the letter grade, then outputs.
    if average >= 90:
        print('Average Test Score:', average, 'A')
    elif average <= 89 and average >= 80:
        print('Average Test Score:', average, 'B')
    elif average <= 79 and average >= 70:
        print('Average Test Score:', average, 'C')
    elif average <= 69 and average >= 60:
        print('Average Test Score:', average, 'D')
    elif average <= 59:
        print('Average Test Score:', average, 'F')
    else:
        print('Error! Please try again!')
    
# Define the required determine_grade function.    
def determine_grade(score):
    
    # Takes the passed score argument and finds the letter grade, then outputs.
    if score >= 90:
        print(score, 'A')
    elif score <= 89 and score >= 80:
        print(score, 'B')
    elif score <= 79 and score >= 70:
        print(score, 'C')
    elif score <= 69 and score >= 60:
        print(score, 'D')
    elif score <= 59:
        print(score, 'F')
    else:
        print('Error! Please try again!')

# Call the main function.
main()