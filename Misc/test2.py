# Defines the main function.
def main():
    
    puzzle = [[0, 1, 2], [0, 1, 2], [0, 1, 2]]
    
for index in range(len(puzzle)):
    
    compList = [column[index] for column in puzzle]
    
    comp = sum(compList) / len(compList)
    
    if (comp != index):
        sentinel = False
        break
    elif (comp == index):
        sentinel = True
    

if (sentinel == False):
    print("The array is not formatted correctly.")
elif (sentinel == True):
    print("The array is formatted correctly.")

# Calls the main function.
main()