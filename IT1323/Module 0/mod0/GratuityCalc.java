/* Name: C. Ryan Smith
 * Date: Sunday, August 25, 2019
 * Course: IT 1323L
 * Assignment: Module 0 Lab
 * Description: Program to calculate gratuity and total for bills.
 * Source: Textbook, pages 25-39.
 */

package mod0;

// Import the java Scanner utility.
import java.util.Scanner;

// Declare the GratuityCalc class;.
public class GratuityCalc {
	public static void main(String[] args) {
		
		// Create scanner object.
		Scanner input = new Scanner(System.in);
		
		// Prompt for subtotal and store in variable.
		System.out.print("Subtotal: $");
		double subtotal = input.nextDouble();
		
		// Prompt for gratuity rate and store in variable.
		System.out.print("Gratuity percentage rate: ");
		double gratrate = input.nextDouble();
		
		// Display the subtotal and gratuity rate.
		System.out.println("\nSubtotal: $" + subtotal);
		System.out.println("Gratuity rate: " + gratrate + "%");
		
		// Calculate gratuity and total due, store in variables.
		double gratuity = gratrate / subtotal;
		double total = gratuity + subtotal;
		
		// Display the gratuity and total due.
		System.out.println("Gratuity: $" + gratuity);
		System.out.println("Total Due: $" + total);
	}
}