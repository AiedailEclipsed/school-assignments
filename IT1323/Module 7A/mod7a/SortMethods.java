/* Name: C. Ryan Smith
 * Date: Friday, November 22, 2019
 * Course: IT 1323L
 * Assignment: Module 7A Lab
 * Description: Program to demonstrate mastery of sorting methods.
 * Source: Textbook.
 */

package mod7a;

// Import the java Scanner utility.
import java.util.Scanner;

// Declare the SortMethods class.
public class SortMethods {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Initialize Scanner object and temperatures array.
		Scanner input = new Scanner(System.in);
		double[][] temps = new double[2][12];
		
		// Loop structure to gather user input and populate array.
		for (int col = 0; col < temps[1].length; col++) {
			System.out.print("Month #" + (col + 1) + ", highest temperature: ");
			temps[0][col] = input.nextDouble();
			
			System.out.print("Month #" + (col + 1) + ", lowest temperature: ");
			temps[1][col] = input.nextDouble();
		}
		
		// Close Scanner object.
		input.close();
		
		// Print unsorted table header.
		System.out.println("\t\tHighest Temp\tLowest Temp" + "\n\t\t----------\t----------");
		
		// Loop structure to print unsorted table.
		for (int col = 0; col < temps[1].length; col++) {
			System.out.println("Month #" + (col + 1) + ": \t     " + temps[0][col] + "\t     " + temps[1][col]);
		}
		
		// Call functions to create new arrays with sorted values of high & low temperatures.
		double[] lowTemps = sortLow(temps[1]);
		double[] highTemps = sortHigh(temps[0]);
		
		// Print lowest temperatures array.
		System.out.println("----------\nLowest temperatures, sorted from low to high\n----------");
		for (int temp = 0; temp < lowTemps.length; temp++) {
			System.out.print(lowTemps[temp] + " ");
		}
		
		// Print highest temperatures array.
		System.out.println("\n----------\nHighest temperatures, sorted from high to low\n----------");
		for (int temp = 0; temp < highTemps.length; temp++) {
			System.out.print(highTemps[temp] + " ");
		}
	}
	
	// Declare the sortLow method, to sort passed array from lowest to highest value and return sorted array to call.
	public static double[] sortLow(double[] array) {
		for (int temp = 0; temp < array.length - 1; temp++) {
			for (int temp2 = temp + 1; temp2 < array.length; temp2++) {
				if (array[temp2] < array[temp]) {
					double sort = array[temp2];
					array[temp2] = array[temp];
					array[temp] = sort;
				}
			}
		}
		
		return array;
	}
	
	// Declare the sortHigh method, to sort passed array from highest to lowest value and return sorted array to call.
	public static double[] sortHigh(double[] array) {
		for (int temp = 0; temp < array.length - 1; temp++) {
			for (int temp2 = temp + 1; temp2 < array.length; temp2++) {
				if (array[temp2] > array[temp]) {
					double sort = array[temp2];
					array[temp2] = array[temp];
					array[temp] = sort;
				}
			}
		}
		
		return array;
	}
}
