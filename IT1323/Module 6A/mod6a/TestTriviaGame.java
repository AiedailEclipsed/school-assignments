/* Name: C. Ryan Smith
 * Date: Friday, November 8, 2019
 * Course: IT 1323L
 * Assignment: Module 6A Lab
 * Description: Program to showcase understanding of classes, inheritance, and linked lists.
 * Source: Textbook.
 */

package mod6a;

// Declare the TestTriviaGame class (client).
public class TestTriviaGame {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		Trivia obj1 = new Trivia("Trivia object #1", 1, 50, 5);
		Trivia obj2 = new Trivia("Trivia object #2", 2, 100, 10);
		Trivia obj3 = new Trivia("Trivia object #3", 3, 250, 15);
		Trivia obj4 = new Trivia("Trivia object #4", 4, 500, 20);
		Trivia obj5 = new Trivia("Trivia object #5", 5, 1000, 25);
		
		System.out.println(obj1.toString() + "\n----------");
		System.out.println(obj2.toString() + "\n----------");
		System.out.println(obj3.toString() + "\n----------");
		System.out.println(obj4.toString() + "\n----------");
		System.out.println(obj5.toString());
	}
}

// Declare the Game class (super).
class Game {
	
	// Initialize the data attributes for the class.
	String description = "Default";
	
	// Constructor methods.
	// Constructor using default values.
	Game(){
	}
	
	// Constructor using user-defined values.
	Game(String newDesc) {
		this.description = newDesc;
	}
	
	// Accessor methods.
	// Defines getDesc method, returns game description.
	String getDesc() {
		return description;
	}
	
	// Mutator methods.
	// Defines setDesc method, sets new game description.
	void setDesc(String newDesc) {
		this.description = newDesc;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return "Game Description: " + description;
	}
}

// Declare the Trivia class (sub).
class Trivia extends Game {
	
	// Initialize the data attributes for the class.
	int gameID = 0;
	double prize = 0.0;
	int questions = 0;
	
	// Constructor methods.
	// Constructor using default values.
	Trivia() {
	}
	
	// Constructor using user-defined values.
	Trivia(String newDesc, int newGID, double newPrize, int newQuestions) {
		super(newDesc);
		this.gameID = newGID;
		this.prize = newPrize;
		this.questions = newQuestions;
	}
	
	// Accessor methods.
	// Defines getGID method, returns trivia game ID.
	int getGID() {
		return gameID;
	}
	
	// Defines getPrize method, returns ultimate prize money.
	double getPrize() {
		return prize;
	}
	
	// Defines getQuestions method, returns number of questions that must be answered to win.
	int getQuestions() {
		return questions;
	}
	
	// Mutator methods.
	// Defines setGID method, sets new trivia game ID.
	void setGID(int newGID) {
		this.gameID = newGID;
	}
	
	// Defines setPrize method, sets new ultimate prize money.
	void setPrize(double newPrize) {
		this.prize = newPrize;
	}
	
	// Defines setQuestions method, sets new number of questions that must be answered to win.
	void setQuestions(int newQuestions) {
		this.questions = newQuestions;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return super.toString() + "\nTrivia Game ID: " + gameID + "\nUltimate Prize Money: $" + prize + "\nNumber of Questions that must be answered to win: " + questions; 
	}
}