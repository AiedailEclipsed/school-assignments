/* Name: C. Ryan Smith
 * Date: Thursday, October 10, 2019
 * Course: IT 1323L
 * Assignment: Module 4A Lab
 * Description: Program to showcase basic IO and exception handling knowledge.
 * Source: Textbook.
 */

package mod4a;

// Import IO & Scanner utilities.
import java.io.*;
import java.util.Scanner;

// Declare the FileImport class.
public class FileImport {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Exception handling block.
		try {
			
			// Input & output file setup.
			File inputFile = new File("input.txt");
			Scanner input = new Scanner(inputFile);
			
			File outputFile = new File("output.txt");
			PrintWriter output = new PrintWriter(outputFile);
			
			// Program opening message.
			System.out.println("Processing file " + inputFile.getName() + " to file " + outputFile.getName() + ".");
			
			// Counter for output file.
			int count = 1;
			
			// While loop to read lines from input file to output file.
			while (input.hasNext()) {
				String line = input.nextLine();
				output.println(count + ". " + line);
				count++;
			}
			
			// Program closing message.
			System.out.println("File processing complete.");
			input.close();
			output.close();
		}
		// If inputfile cannot be found, throw an error.
		catch (FileNotFoundException ex) {
			System.out.println("Error: File not found.");
		}
	}
}
