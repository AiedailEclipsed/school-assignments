/* Name: C. Ryan Smith
 * Date: Friday, November 8, 2019
 * Course: IT 1323L
 * Assignment: Module 6B Lab
 * Description: Program to showcase understanding of classes, inheritance, and linked lists.
 * Source: Textbook.
 */

package mod6b;

import java.util.LinkedList;

// Declare the TestLinkedList class (client).
public class TestLinkedList {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Create a blank TriviaLinkedList.
		TriviaLinkedList list = new TriviaLinkedList();
		
		// Create 5 Trivia objects.
		Trivia obj1 = new Trivia("Trivia object #1", 1, 50, 5);
		Trivia obj2 = new Trivia("Trivia object #2", 2, 100, 10);
		Trivia obj3 = new Trivia("Trivia object #3", 3, 250, 15);
		Trivia obj4 = new Trivia("Trivia object #4", 4, 500, 20);
		Trivia obj5 = new Trivia("Trivia object #5", 5, 1000, 25);
		
		// Insert trivia objects to TriviaLinkedList and print results.
		list.insert(obj1);
		list.toString(list);
		list.insert(obj2);
		list.toString(list);
		list.insert(obj3);
		list.toString(list);
		list.insert(obj4);
		list.toString(list);
		list.insert(obj5);
		list.toString(list);
		
		// Remove the first trivia object in the list & print results.
		list.delete(0);
		list.toString(list);
		
		// Remove out of bounds trivia object & print results.
		list.delete(4);
		list.toString(list);
	}
}

// Declare the TriviaLinkedList class.
class TriviaLinkedList {
	
	// Create Node subclass of Trivia type.
	class Node <Trivia> {
		
		// Initialize the data attributes for the Node subclass.
		Trivia game;
		Node <Trivia> next;
		
		// Constructor methods.
		// Constructor using default values.
		Node() {
		}
		
		// Constructor using user-defined values.
		Node(Trivia newGame, Node<Trivia> nextValue) {
			game = newGame;
			next = nextValue;
		}
	}
	
	// Initialize the data attributes for the class.
	Node <Trivia> head, tail;
	int size = 0;
	
	// Constructor method.
	TriviaLinkedList() {
		head = null;
	}
	
	// insert method, inserts a new trivia object to list at head, increases size attribute of list.
	void insert(Trivia newGame) {
		head = new Node <Trivia>(newGame, head);
		size++;
	}
	
	// toString method, prints out the list of trivia objects.
	void toString(TriviaLinkedList list) {
		Node <Trivia> position = list.head;
		
		System.out.print("Trivia List \n---------- \n");
		
		while (position != null) {
			System.out.print(position.game + "\n---\n");
			
			position = position.next;
		}
	}
	
	// delete method, removes a trivia object based on the passed id of the node.
	void delete(int id) {
		if (id < 0 || id >= size) {
			System.out.println("Deletion unsuccessful; out of range of list.\n---");
		}
		else if (id == 0) {
			if (size == 0) {
				System.out.println("Deletion unsuccessful; nothing to remove.\n---");
			}
			
			head = head.next;
			size--;
			
			if (head == null) {
				tail = null;
			}
			
			System.out.println("Deletion successful.\n---");
		}
		else if (id == size - 1) {
			if (size == 0) {
				System.out.println("Deletion unsuccessful; nothing to remove.\n---");
			}
			else if (size == 1) {
				head = tail = null;
				size = 0;
				
				System.out.println("Deletion successful.\n---");
			}
			else {
				Node <Trivia> current = head;
				
				for (int temp = 0; temp < size - 2; temp++) {
					current = current.next;
				}
				
				tail = current;
				tail.next = null;
				size--;
				
				System.out.println("Deletion successful.\n---");
			}
		}
		else {
			Node <Trivia> previous = head;
			
			for (int temp = 1; temp < id; temp++) {
				previous = previous.next;
			}
			
			Node <Trivia> current = previous.next;
			previous.next = current.next;
			size--;
			
			System.out.println("Deletion successful.\n---");
		}
	}
}

//Declare the Game class (super).
class Game {
	
	// Initialize the data attributes for the class.
	String description = "Default";
	
	// Constructor methods.
	// Constructor using default values.
	Game(){
	}
	
	// Constructor using user-defined values.
	Game(String newDesc) {
		this.description = newDesc;
	}
	
	// Accessor methods.
	// Defines getDesc method, returns game description.
	String getDesc() {
		return description;
	}
	
	// Mutator methods.
	// Defines setDesc method, sets new game description.
	void setDesc(String newDesc) {
		this.description = newDesc;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return "Game Description: " + description;
	}
}

//Declare the Trivia class (sub).
class Trivia extends Game {
	
	// Initialize the data attributes for the class.
	int gameID = 0;
	double prize = 0.0;
	int questions = 0;
	
	// Constructor methods.
	// Constructor using default values.
	Trivia() {
	}
	
	// Constructor using user-defined values.
	Trivia(String newDesc, int newGID, double newPrize, int newQuestions) {
		super(newDesc);
		this.gameID = newGID;
		this.prize = newPrize;
		this.questions = newQuestions;
	}
	
	// Accessor methods.
	// Defines getGID method, returns trivia game ID.
	int getGID() {
		return gameID;
	}
	
	// Defines getPrize method, returns ultimate prize money.
	double getPrize() {
		return prize;
	}
	
	// Defines getQuestions method, returns number of questions that must be answered to win.
	int getQuestions() {
		return questions;
	}
	
	// Mutator methods.
	// Defines setGID method, sets new trivia game ID.
	void setGID(int newGID) {
		this.gameID = newGID;
	}
	
	// Defines setPrize method, sets new ultimate prize money.
	void setPrize(double newPrize) {
		this.prize = newPrize;
	}
	
	// Defines setQuestions method, sets new number of questions that must be answered to win.
	void setQuestions(int newQuestions) {
		this.questions = newQuestions;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return super.toString() + "\nTrivia Game ID: " + gameID + "\nUltimate Prize Money: $" + prize + "\nNumber of Questions that must be answered to win: " + questions; 
	}
}