/* Name: C. Ryan Smith
 * Date: Sunday, August 25, 2019
 * Course: IT 1323L
 * Assignment: Module 1A Lab
 * Description: Program to operate on arrays and return values.
 * Source: Textbook (chapter 6 & 7), website (https://www.timeanddate.com/weather/@4317412/historic) for temperatures.
 */

package mod1a;

// Declare the TempFunctions class.
public class TempFunctions {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Create the initial temperature list.
		int[] temperatureList = {90, 84, 88, 84, 91, 86, 90};
		
		System.out.println("High temperatures (Farenheit) for Bogalusa, LA last week" + "\n---------------------------");
		
		// Print output based on day of week corresponding to array location.
		for (int temp = 0; temp < temperatureList.length; temp++) {
			if (temp == 0) {
				System.out.println("Sunday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 1) {
				System.out.println("Monday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 2) {
				System.out.println("Tuesday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 3) {
				System.out.println("Wednesday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 4) {
				System.out.println("Thursday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 5) {
				System.out.println("Friday: " + temperatureList[temp] + " degrees");
			}
			else if (temp == 6) {
				System.out.println("Saturday: " + temperatureList[temp] + " degrees");
			}
		}
		
		// Call minTemp and maxTemp functions, print return values.
		System.out.println("---------------------------");
		System.out.println("Lowest temperature: " + minTemp(temperatureList) + " degrees");
		System.out.println("Highest temperature: " + maxTemp(temperatureList) + " degrees" + "\n---------------------------");
		System.out.println("Forecasted high temperatures (Farenheit) for Bogalusa, LA next week" + "\n---------------------------");
		
		// Create new array based on return of incTemp function.
		int[] incTempList = incTemp(temperatureList);
		
		// Print output of new array.
		for (int temp = 0; temp < incTempList.length; temp++) {
			if (temp == 0) {
				System.out.println("Sunday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 1) {
				System.out.println("Monday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 2) {
				System.out.println("Tuesday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 3) {
				System.out.println("Wednesday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 4) {
				System.out.println("Thursday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 5) {
				System.out.println("Friday: " + incTempList[temp] + " degrees");
			}
			else if (temp == 6) {
				System.out.println("Saturday: " + incTempList[temp] + " degrees");
			}
		}
	}
	
	// Declare the minTemp function.
	public static int minTemp(int[] temperatureList) {
		
		// Operation to find smallest value in array.
		int min = temperatureList[0];
		
		for (int temp = 1; temp < temperatureList.length; temp++) {
			if (temperatureList[temp] < min) {
				min = temperatureList[temp];
			}
		}
		
		// Returns the found value to function call.
		return min;
	}
	
	
	// Declare the maxTemp function.
	public static int maxTemp(int[] temperatureList) {
		
		// Operation to find largest value in array.
		int max = temperatureList[0];
		
		for (int temp = 1; temp < temperatureList.length; temp++) {
			if (temperatureList[temp] > max) {
				max = temperatureList[temp];
			}
		}
		
		// Returns the found value to function call.
		return max;
	}
	
	
	// Declare the incTemp function.
	public static int[] incTemp(int[] temperatureList) {
		
		// Create new array to store increased values, operation to increase values of existing array, and store them in new.
		int[] incTempList = new int[temperatureList.length];
		
		for (int temp = 0; temp < temperatureList.length; temp++) {
			incTempList[temp] = temperatureList[temp] + 10;
		}
		
		// Returns the new filled array to function call.
		return incTempList;
	}
}
