/* Name: C. Ryan Smith
 * Date: Tuesday October 29, 2019
 * Course: CSE2300 (Discrete Structures)
 * Assignment: Course Project - Selection Sort #2
 */

package dsproject;

import java.util.Scanner;

public class SSort2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Selection Sort #2" + "\n----------");
		System.out.print("Please enter a whole number: ");
		int n = input.nextInt();
		double runningTime = 0;
		int itemTotal = 0;
		
		for (int aLoop = 0; aLoop < 1000; aLoop++) {
			int[] array = new int[n];
			
			for (int temp = 0; temp < array.length; temp++) {
				array[temp] = (int)(Math.random() * 50000);
			}
			
			double startTime = System.currentTimeMillis();
			
			for (int temp = 0; temp < array.length - 1; temp++) {
				int min = temp;
				for (int temp2 = temp + 1; temp2 < array.length; temp2++) {
					if (array[temp2] < array[min]) {
						min = temp2;
					}
				}
				if (temp != min && min < array.length) {
					int sort = array[min];
					array[min] = array[temp];
					array[temp] = sort;
				}
			}
			
			double endTime = System.currentTimeMillis();
			runningTime = runningTime + (endTime - startTime);
			itemTotal = itemTotal + array.length;
		}
		
		double avgRunTime = runningTime / 1000;
		System.out.println("----------" + "\nNumber of items sorted: " + itemTotal);
		System.out.println("Average run time per array sorted: " + avgRunTime + " milliseconds");
		
		input.close();
	}
}
