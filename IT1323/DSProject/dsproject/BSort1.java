/* Name: C. Ryan Smith
 * Date: Tuesday October 29, 2019
 * Course: CSE2300 (Discrete Structures)
 * Assignment: Course Project - Bubble Sort #1
 */

package dsproject;

import java.util.Scanner;

public class BSort1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Bubble Sort #1" + "\n----------");
		System.out.print("Please enter a whole number: ");
		int userNumber = input.nextInt();
		
		int[] array = new int[userNumber];
		for (int temp = 0; temp < array.length; temp++) {
			array[temp] = (int)(Math.random() * 5000);
		}
		
		System.out.println("----------" + "\nUnsorted Array:");
		
		for (int temp = 0; temp < array.length; temp++) {
			System.out.print(array[temp] + " ");
		}
		
		for (int temp = 0; temp < array.length - 1; temp++) {
			for (int temp2 = temp + 1; temp2 < array.length; temp2++) {
				if (array[temp2] < array[temp]) {
					int sort = array[temp2];
					array[temp2] = array[temp];
					array[temp] = sort;
				}
			}
		}
		
		System.out.println("\n----------" + "\nSorted Array:");
		
		for (int temp = 0; temp < array.length; temp++) {
			System.out.print(array[temp] + " ");
		}
		
		input.close();
	}
}
