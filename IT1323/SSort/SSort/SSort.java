// Name: C. Ryan Smith

package SSort;

import java.util.Scanner;

public class SSort {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Selection Sort" + "\n----------");
		System.out.print("Please enter a whole number: ");
		int userNumber = input.nextInt();
		
		int[] array = new int[userNumber];
		for (int temp = 0; temp < array.length; temp++) {
			array[temp] = (int)(Math.random() * 100);
		}
		
		System.out.println("----------" + "\nUnsorted Array:");
		
		for (int temp = 0; temp < array.length; temp++) {
			System.out.print(array[temp] + " ");
		}
		
		for (int temp = 0; temp < array.length - 1; temp++) {
			int min = temp;
			for (int temp2 = temp + 1; temp2 < array.length; temp2++) {
				if (array[temp2] < array[min]) {
					min = temp2;
				}
			}
			if (temp != min && min < array.length) {
				int sort = array[min];
				array[min] = array[temp];
				array[temp] = sort;
			}
		}
		
		System.out.println("\n----------" + "\nSorted Array:");
		
		for (int temp = 0; temp < array.length; temp++) {
			System.out.print(array[temp] + " ");
		}
		
		input.close();
	}
}
