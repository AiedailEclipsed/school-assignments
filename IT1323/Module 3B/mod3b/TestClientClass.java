/* Name: C. Ryan Smith
 * Date: Thursday, October 4, 2019
 * Course: IT 1323L
 * Assignment: Module 3B Lab
 * Description: Program to create and perform operations with classes and inheritance.
 * Source: Textbook.
 */

package mod3b;

//Import the java Scanner utility.
import java.util.Scanner;

// Declare the TestClientClass class (client).
public class TestClientClass {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Create totalDue variable.
		double totalDue = 0;
		
		// Create array of hourly clients.
		HourlyClient[] clientArray = new HourlyClient[5];
		
		// Loop to capture data to initialize object array.
		for (int temp = 0; temp < clientArray.length; temp++) {
			
			// Create scanner object.
			Scanner input = new Scanner(System.in);
			
			System.out.println("Please input information for client #" + (temp + 1) + " as instructed below. \n-----------------");
			System.out.print("Company Name: ");
			String newName = input.nextLine();
			System.out.print("Company ID: ");
			String newID = input.nextLine();
			System.out.print("Street Address: ");
			String newAddress = input.nextLine();
			System.out.print("City: ");
			String newCity = input.nextLine();
			System.out.print("State: ");
			String newState = input.nextLine();
			System.out.print("Hourly rate charged: $");
			double newRate = input.nextDouble();
			System.out.print("Hours billed: ");
			double newHours = input.nextDouble();
			System.out.println("-----------------");
			
			clientArray[temp] = new HourlyClient(newName, newID, newAddress, newCity, newState, newRate, newHours);
		}
		
		// Loop to print all data from object array.
		for (int temp = 0; temp < clientArray.length; temp++) {
			System.out.println("Client #" + (temp + 1) + "\n-----------------");
			System.out.print(clientArray[temp].toString() + "-----------------\n");
		}
		
		// Loop to print billing dues for array objects & find the total due for all objects.
		for (int temp = 0; temp < clientArray.length; temp++) {
			System.out.println("Amount due for " + clientArray[temp].name + ": $" + clientArray[temp].amtDue());
			
			totalDue = clientArray[temp].amtDue() + totalDue;
		}
		
		// Print the total due for all objects.
		System.out.println("Total amount due for all clients: $" + totalDue);
	}
}

// Declare the Client class (super).
class Client {
	
	// Initialize the data attributes for the class.
	String name = "Default";
	String id = "Default";
	String address = "Default";
	String city = "Default";
	String state = "Default";
	
	// Constructor methods.
	// Constructor using default values.
	Client() {
	}
		
	// Constructor using user-defined values.
	Client(String newName, String newID, String newAddress, String newCity, String newState) {
		this.name = newName;
		this.id = newID;
		this.address = newAddress;
		this.city = newCity;
		this.state = newState;
	}
	
	// Defines the toString function, prints data attributes for specified object.
	public String toString() {
		return "Company Name: " + name + "\nCompany ID: " + id + "\nStreet Address: " + address + "\nCity: " + city + "\nState: " + state;
	}
}

// Declare the HourlyClient class (sub).
class HourlyClient extends Client {
	
	// Initialize the data attributes for the class.
	double hourlyRate = 0;
	double hoursBilled = 0;
	
	// Constructor methods.
	// Constructor using default values.
	HourlyClient() {
	}
		
	// Constructor using user-defined values.
	HourlyClient(String newName, String newID, String newAddress, String newCity, String newState, double newRate, double newHours) {
		super(newName, newID, newAddress, newCity, newState);
		this.hourlyRate = newRate;
		this.hoursBilled = newHours;
	}
	
	// Defines the toString function, prints data attributes for specified object.
	public String toString() {
		return super.toString() + "\nHourly Rate: $" + hourlyRate + "\nHours Billed: " + hoursBilled + "\nAmount Due: $" + this.amtDue() + "\n";
	}
	
	// Defines the amtDue function, returns the amount due given the hourly rate and hours worked.
	double amtDue() {
		double amtDue = hourlyRate * hoursBilled;
		
		return amtDue;
	}
}