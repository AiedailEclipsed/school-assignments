/* Name: C. Ryan Smith
 * Date: Friday, September 6, 2019
 * Course: IT 1323L
 * Assignment: Module 1B Lab
 * Description: Program to operate on multidimensional arrays and return values.
 * Source: Textbook (chapter 4, section 6 & chapters 7, 8).
 */

package mod1b;

// Declare the BookstoreSales class.
public class BookstoreSales {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Create the initial multidimensional array.
		double[][] sales = {
				{1234.55, 2222.35, 2400.15, 2523.73, 567.84},
				{1001.13, 1101.01, 923.92, 907.57, 250.15},
				{732.87, 494.15, 502.85, 323.50, 214.17},
				{551.15, 442.25, 383.89, 627.19, 239.18},
		};
		
		// Print the table header.
		System.out.println("\tMonday\t\tTuesday\t\tWednesday\tThursday\tFriday");
		
		// Print and format the array as a table.
		for (int row = 0; row < sales.length; row++) {
			System.out.print("Week " + (row + 1) + ":\t");
			for (int column = 0; column < sales[row].length; column++) {
				System.out.printf("%6.2f", sales[row][column]);
				System.out.print("\t\t");
			}
			System.out.println();
		}
		
		// Call the averageSales function, print returned value formatted.		
		System.out.printf("Average sales for the month: $%6.2f", averageSales(sales));
		System.out.println();
		
		// Call the weekSales function, print return values formatted.
		for (int temp = 0; temp < sales.length; temp++) {
			System.out.print("Total week " + (temp + 1) + " sales: ");
			System.out.printf("$%6.2f", weekSales(sales, temp));
			System.out.println();
		}	
	}
	
	// Declare the weekSales function.
	public static double weekSales(double[][] sales, int week) {
		
		// Operation to add week sales based on the passed variable.
		double total = 0;
		
		for (int temp = 0; temp < sales[week].length; temp++) {
			total += sales[week][temp];
		}
		
		// Returns the found value to function call.
		return total;
	}
	
	// Declare the averageSales function.
	public static double averageSales(double [][] sales) {
		
		// Operation to find total of entire array.
		double total = 0;
		
		for (int row = 0; row < sales.length; row++) {
			for (int column = 0; column < sales[row].length; column++) {
				total += sales[row][column];
			}
		}
		
		// Operation to use found total to find the average, returns value to function call.
		double average = total / sales.length;
		
		return average;
	 }
}
