/* Name: C. Ryan Smith
 * Date: Thursday, September 26, 2019
 * Course: IT 1323L
 * Assignment: Module 3A Lab
 * Description: Program to create and perform operations with classes and inheritance.
 * Source: Textbook.
 */

package mod3a;

// Declare the TestSuperClass class (client).
public class TestSuperClass {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Create & test Store object and methods.
		Store testStore = new Store();
		System.out.println("Store object with default values.");
		System.out.println(testStore.toString());
		testStore.setName("Wal-Mart");
		testStore.setTaxRate(14);
		System.out.println("Store object with changed values.");
		System.out.println("Store name is now " + testStore.getName() + ".");
		System.out.println("Tax rate is now " + testStore.getTaxRate() + "%");
		
		// Create & test 1st Yarn object and methods.
		Yarn obj1 = new Yarn();
		System.out.println("\nYarn object #1 with default values.");
		System.out.println(obj1.toString());
		obj1.setName("Yarn Emporium");
		obj1.setTaxRate(15);
		obj1.setSoldItems(10);
		obj1.setAvgPrice(7);
		System.out.println("Yarn object #1 with changed values.");
		System.out.println("Store name is now " + obj1.getName() + ".");
		System.out.println("Tax rate is now " + obj1.getTaxRate() + "%");
		System.out.println("Number of items sold is now " + obj1.getSoldItems() + ".");
		System.out.println("Average price of items sold is now $" + obj1.getAvgPrice());
		
		// Create & test 2nd Yarn object and methods.
		Yarn obj2 = new Yarn("Yarnopolis", 10, 50, 5);
		System.out.println("\nYarn object #2 with user-defined values.");
		System.out.println(obj2.toString());
		System.out.println("Taxes paid for total items sold: $" + obj2.avgTax());
		System.out.println("Is the content of object #1 the same as object #2? " + obj2.equals(obj1));
	}
}

// Declare the Store class (super).
class Store {
	
	// Initialize data attributes for the class.
	String name = "Default";
	double taxRate = 0;
	
	// Constructor methods.
	// Constructor using default values.
	Store() {
	}
	
	// Constructor using user-defined values.
	Store(String newName, double newTaxRate) {
		this.name = newName;
		this.taxRate = newTaxRate;
	}
	
	// Accessor methods.
	// Defines getName method, returns store name.
	String getName() {
		return name;
	}
	
	// Defines getTaxRate method, returns tax rate.
	double getTaxRate() {
		return taxRate;
	}
	
	// Mutator methods.
	// Defines setName method, sets new store name.
	void setName(String newName) {
		this.name = newName;
	}
	
	// Defines setTaxRate method, sets new tax rate.
	void setTaxRate(double newTaxRate) {
		this.taxRate = newTaxRate;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return "Store Name: " + name + "\nTax Rate: " + taxRate + "%";
	}
}

// Declare the Yarn class (sub).
class Yarn extends Store {
	
	// Initialize data attributes for the class.
	int soldItems = 0;
	double avgPrice = 0;
	
	// Constructor methods.
	// Constructor using default values.
	Yarn() {
	}
	
	// Constructor using user-defined values.
	Yarn(String newName, double newTax, int items, double price) {
		super(newName, newTax);
		this.soldItems = items;
		this.avgPrice = price;
	}
	
	// Accessor methods.
	// Defines getSoldItems method, returns number of sold items.
	double getSoldItems() {
		return soldItems;
	}
	
	// Defines getAvgPrice method, returns average price of sold items.
	double getAvgPrice() {
		return avgPrice;
	}
	
	// Mutator methods.
	// Defines setSoldItems method, sets new number of items sold.
	void setSoldItems(int items) {
		this.soldItems = items;
	}
	
	// Defines setAvgPrice method, sets new average price of sold items.
	void setAvgPrice(double price) {
		this.avgPrice = price;
	}
	
	// Defines toString method, prints data attributes for specified class.
	public String toString() {
		return super.toString() + "\nSold Items: " + soldItems + "\nAverage Price: $" + avgPrice; 
	}
	
	// Defines equals method, comparing two objects contents to see if they are identical.
	public boolean equals(Object comp) {
		if (comp instanceof Yarn)
			if (this.name == ((Store) comp).getName())
				if (this.taxRate == ((Store) comp).getTaxRate())
					if (this.soldItems == getSoldItems())
						if (this.avgPrice == getAvgPrice())
							return true;
						else
							return false;
					else
						return false;
				else
					return false;
			else
				return false;
		else
			return false;
	}
	
	// Defines avgTax method, returns the average taxes for the store.
	double avgTax() {
		double avgTax = ((soldItems * avgPrice) * (taxRate * .01));
		
		return avgTax;
	}
}