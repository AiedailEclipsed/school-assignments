/* Name: C. Ryan Smith
 * Date: Tuesday, September 10, 2019
 * Course: IT 1323L
 * Assignment: Module 2A Lab
 * Description: Program to create and perform basic operations using classes.
 * Source: Textbook ().
 */

package mod2a;

import java.util.Arrays;

// Declare the TestKSUVendor class (client).
public class TestKSUVendor {
	
	// Declare the main function.	
	public static void main(String[] args) {
		
		// Create first test object using default values.
		KSUVendor vendor1 = new KSUVendor();
		System.out.println("Testing object 1, with default values:");
		System.out.println(vendor1.toString());
		vendor1.setName("Burger King");
		vendor1.setID(12345);
		vendor1.changeQP(vendor1.getQP(), 0, 5);
		vendor1.changeQP(vendor1.getQP(), 1, 6);
		vendor1.changeQP(vendor1.getQP(), 2, 7);
		vendor1.changeQP(vendor1.getQP(), 3, 8);
		System.out.println("Changed values of object 1:");
		System.out.println(vendor1.toString());
		System.out.println("Total purchase orders of object 1: " + vendor1.totalQP(vendor1.getQP()));
		
		// Create second test object using user-defined values.
		KSUVendor vendor2 = new KSUVendor("Chick-fil-A", 67890, new double[] {1,2,3,4});
		System.out.println("\nTesting object 2, with user-defined values:");
		System.out.println(vendor2.toString());
		System.out.println("Vendor name: " + vendor2.getName());
		System.out.println("Vendor ID: " + vendor2.getID());
		System.out.println("Vendor purchases: " + Arrays.toString(vendor2.getQP()));
	}
}

// Declare the KSUVendor class
class KSUVendor {
	
	// Initialize data attributes for class.
	String comName = "Default";
	int ID = 0;
	double[] quartPurchase = new double[4];
	
	// Constructor methods.
	// Constructor using default values.
	KSUVendor() {
	}
	
	// Constructor using user-defined values.
	KSUVendor(String newName, int newID, double[] newQP) {
		comName = newName;
		ID = newID;
		quartPurchase = newQP;
	}
	
	// Accessor methods.
	// Defines getName method, returns the company name.
	String getName() {
		return comName;
	}
	
	// Defines getID method, returns the company ID.
	int getID() {
		return ID;
	}
	
	// Defines getQP method, returns the quarterly purchase array.
	double[] getQP() {
		return quartPurchase;
	}
	
	// Mutator methods.
	// Defines setName method, changes company name based on passed string.
	void setName(String newName) {
		comName = newName;
	}
	
	// Defines setID method, change company ID based on passed integer value.
	void setID(int newID) {
		ID = newID;
	}
	
	// toString method, prints data attributes for specified class.
	public String toString() {
		return "Company name: " + comName + "\nCompany ID: " + ID + "\nQuarterly purchases: " + Arrays.toString(quartPurchase);
	}
	
	// Defines the totalQP method, returning the total sum of the quarterly purchases.
	double totalQP(double[] quartPurchase) {
		double total = 0;
		
		for (int temp = 0; temp < quartPurchase.length; temp++) {
			total += quartPurchase[temp];
		}
		
		return total;
	}
	
	// Defines the changeQP method, allowing modification of the quartPurchase array (also serves as mutator).
	void changeQP(double[] quartPurchase, int mod, int newQP) {
		quartPurchase[mod] = newQP;
	}
}