/* Name: C. Ryan Smith
 * Date: Friday, December 6, 2019
 * Course: IT 1323L
 * Assignment: Final Lab
 */

package jfinal;

//Import IO & Scanner utilities.
import java.io.*;
import java.util.Scanner;

// Declare the Final class.
public class Final {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Exception handling block.
		try {
		
			// Input & output file setup.
			File inputFile = new File("input.txt");
			Scanner input = new Scanner(inputFile);
						
			File outputFile = new File("output.txt");
			PrintWriter output = new PrintWriter(outputFile);
			
			// Program opening message.
			System.out.println("Processing file " + inputFile.getName() + " to file " + outputFile.getName() + ".");
			
			// Array setup & while loop to read data from input.txt to array.
			int[] rainfallList = new int[6];
			int count = 0;
			while (input.hasNext()) {
				rainfallList[count] = input.nextInt();
				count++;
			}
			
			// Create new array to store sorted values of rainfallList.
			int[] sortedRain = sort(rainfallList);
			
			// Write new array to output.txt
			output.print("Rainfall (in inches) from high to low: ");
			for (int temp = 0; temp < sortedRain.length; temp++) {
				output.print(sortedRain[temp] + " ");
			}
			
			// Write average & lowest rainfall value to output.txt
			output.printf("\nAverage rainfall: %4.2f inches", findAvg(rainfallList));
			output.println("\nLowest rainfall: " + findMin(rainfallList) + " inches");
			
			// Program closing message.
			System.out.println("File processing complete.");
			input.close();
			output.close();
		}
		
		// Catch statement for FileNotFound error.
		catch (FileNotFoundException ex) {
			System.out.println("Error: File not found.");
		}
	}
	
	// Declare the sort function.
	public static int[] sort(int[] rainfallList) {
		
		// Create & populate new array to sort rainfallList without altering original. 
		int[] sortList = new int[rainfallList.length];
		for (int temp = 0; temp < rainfallList.length; temp++) {
			sortList[temp] = rainfallList[temp];
		}
		
		// Sort duplicate array using selection sort.
		for (int temp = 0; temp < sortList.length - 1; temp++) {
			int min = temp;
			for (int temp2 = temp + 1; temp2 < sortList.length; temp2++) {
				if (sortList[temp2] > sortList[min]) {
					min = temp2;
				}
			}
			if (temp != min && min < sortList.length) {
				int sort = sortList[min];
				sortList[min] = sortList[temp];
				sortList[temp] = sort;
			}
		}
		
		// Returns the new sorted array to function call.
		return sortList;
	}
	
	// Declare the findAvg function.
	public static double findAvg(int[] rainfallList) {
		
		// Operate to find the average of the values in the array.
		double sum = 0;
		for (int temp = 0; temp < rainfallList.length; temp++) {
			sum += rainfallList[temp];
		}
		
		double avg = sum / 6;
		
		// Returns the found value to function call.
		return avg;
	}
	
	// Declare the findMin function.
	public static int findMin(int[] rainfallList) {
		
		// Operation to find smallest value in array.
		int min = rainfallList[0];
		
		for (int temp = 1; temp < rainfallList.length; temp++) {
			if (rainfallList[temp] < min) {
				min = rainfallList[temp];
			}
		}
		
		// Returns the found value to function call.
		return min;
	}
}
