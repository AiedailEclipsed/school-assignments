/* Name: C. Ryan Smith
 * Date: Saturday, October 26, 2019
 * Course: IT 1323L
 * Assignment: Module 5A Lab
 * Description: Program modify existing code to use IO functions..
 * Source: Textbook.
 */

package mod5a;

// Import the Arrays utility.
import java.util.Arrays;

// Declare the TestRecursion class.
public class TestRecursion {
	
	// Declare the main method.
	public static void main(String[] args) {
		
		// Create first test object using default values.
		Recursion test1 = new Recursion();
		System.out.println("Testing object 1, with default values:");
		System.out.println(test1.toString());
		test1.setArray(new int[] {1, 2, 3, 4, 5});
		System.out.println("Changed values of object 1:");
		System.out.println(test1.toString());
		System.out.println("Sum of values in object 1: " + test1.arraySum(0, test1.getArray().length));
		
		// Create second test object using user-defined values.
		Recursion test2 = new Recursion(new int[] {14, 27, 33, 45, 0});
		System.out.println("\nTesting object 2, with user-defined values:");
		System.out.println(test2.toString());
		test2.setElement(4, 58);
		System.out.println("Changed values of object 1:");
		System.out.println("Array: " + Arrays.toString(test2.getArray()));
		System.out.println("Is the content of object #1 the same as object #2? " + test2.equals(test1));
	}
}

// Declare the Recursion class.
class Recursion {
	int[] array = new int[5];
	
	// Constructor methods.
	// Constructor using default values.
	Recursion() {
	}
	
	// Constructor using user-defined values.
	Recursion(int[] newArray) {
		array = newArray;
	}
	
	// Accessor method.	
	// Defines getArray method, returns the array.
	int[] getArray() {
		return array;
	}
	
	// Mutator methods.
	// Defines the setArray method, allowing modification of entire array.
	void setArray(int[] newArray) {
		array = newArray;
	}
	
	// Defines the setElement method, allowing modification of individual array elements.
	void setElement(int mod, int newElement) {
		array[mod] = newElement;
	}
	
	// toString method, prints data attributes for specified class.
	public String toString() {
		return "Array: " + Arrays.toString(array);
	}
	
	// Defines arraySum method, returns the sum of all elements in the array.
	int arraySum(int sum, int length) {	
		if (length != 0) {
			sum = array[length - 1] + arraySum(sum, length - 1);
		}
		
		return sum;
	}
	
	// Defines equals method, comparing two objects contents to see if they are identical.
	public boolean equals(Object comp) {
		if (comp instanceof Recursion)
			if (this.array == ((Recursion) comp).getArray())
				return true;
			else
				return false;
		else
			return false;
	}
}