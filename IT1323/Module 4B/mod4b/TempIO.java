/* Name: C. Ryan Smith
 * Date: Saturday, October 19, 2019
 * Course: IT 1323L
 * Assignment: Module 4B Lab
 * Description: Program modify existing code to use IO functions..
 * Source: Textbook.
 */

package mod4b;

//Import IO & Scanner utilities.
import java.io.*;
import java.util.Scanner;

// Declare the TempIO class.
public class TempIO {
	
	// Declare the main function.
	public static void main(String[] args) {
		
		// Exception handling block.
		try {
		
			// Input & output file setup.
			File inputFile = new File("temps.txt");
			Scanner input = new Scanner(inputFile);
						
			File outputFile = new File("output.txt");
			PrintWriter output = new PrintWriter(outputFile);
			
			// Program opening message.
			System.out.println("Processing file " + inputFile.getName() + " to file " + outputFile.getName() + ".");
			
			// Array setup & while loop to read data from temps.txt to array.
			int[] temperatureList = new int[7];
			int count = 0;
			while (input.hasNext()) {
				temperatureList[count] = input.nextInt();
				count++;
			}
			
			// Write lowest and highest temperature to output.txt
			output.println("Lowest temperature: " + minTemp(temperatureList) + " degrees");
			output.println("Highest temperature: " + maxTemp(temperatureList) + " degrees");
			
			// Create new array based on return of incTemp function.
			int[] incTempList = incTemp(temperatureList);
			
			// Write new array to output.txt
			output.print("Forecasted high temperatures for next week: ");
			for (int temp = 0; temp < incTempList.length; temp++) {
				output.print(incTempList[temp] + " ");
			}
			
			// Program closing message.
			System.out.println("File processing complete.");
			input.close();
			output.close();
		}
		
		// Catch statement for FileNotFound error.
		catch (FileNotFoundException ex) {
			System.out.println("Error: File not found.");
		}
	}
	
	// Declare the minTemp function.
	public static int minTemp(int[] temperatureList) {
		
		// Operation to find smallest value in array.
		int min = temperatureList[0];
		
		for (int temp = 1; temp < temperatureList.length; temp++) {
			if (temperatureList[temp] < min) {
				min = temperatureList[temp];
			}
		}
		
		// Returns the found value to function call.
		return min;
	}
	
	
	// Declare the maxTemp function.
	public static int maxTemp(int[] temperatureList) {
		
		// Operation to find largest value in array.
		int max = temperatureList[0];
		
		for (int temp = 1; temp < temperatureList.length; temp++) {
			if (temperatureList[temp] > max) {
				max = temperatureList[temp];
			}
		}
		
		// Returns the found value to function call.
		return max;
	}
	
	
	// Declare the incTemp function.
	public static int[] incTemp(int[] temperatureList) {
		
		// Create new array to store increased values, operation to increase values of existing array, and store them in new.
		int[] incTempList = new int[temperatureList.length];
		
		for (int temp = 0; temp < temperatureList.length; temp++) {
			incTempList[temp] = temperatureList[temp] + 10;
		}
		
		// Returns the new filled array to function call.
		return incTempList;
	}
}