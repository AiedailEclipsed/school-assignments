/* Name: C. Ryan Smith
 * Date: Friday, November 1, 2019
 * Course: IT 1323L
 * Assignment: Module 5B Lab
 * Description: Program to showcase understanding of abstract class theory and application.
 * Source: Textbook.
 */

package mod5b;

//Import the java Scanner utility.
import java.util.Scanner;

public class AbstractClass {
	public static void main(String[] args) {
		
		// Create array of hourly clients.
		HourlyEmployee[] employeeArray = new HourlyEmployee[2];
		
		// Loop to capture data to initialize object array.
		for (int temp = 0; temp < employeeArray.length; temp++) {
			
			// Create scanner object.
			Scanner input = new Scanner(System.in);
			
			// Accept input data for object.
			System.out.println("Please input information for employee #" + (temp + 1) + " as instructed below. \n-----------------");
			System.out.print("First Name: ");
			String newFirst = input.nextLine();
			System.out.print("Last Name: ");
			String newLast = input.nextLine();
			System.out.print("Employee ID: ");
			String newID = input.nextLine();
			System.out.print("Street Address: ");
			String newAddress = input.nextLine();
			System.out.print("City: ");
			String newCity = input.nextLine();
			System.out.print("State: ");
			String newState = input.nextLine();
			System.out.print("Hourly Wage: $");
			double newRate = input.nextDouble();
			System.out.print("Hours Worked: ");
			double newHours = input.nextDouble();
			System.out.println("-----------------");
			
			// Create & assign object to place in array.
			employeeArray[temp] = new HourlyEmployee(newFirst, newLast, newID, newAddress, newCity, newState, newRate, newHours);
		}
		
		// Loop to print all data from object array.
		for (int temp = 0; temp < employeeArray.length; temp++) {
			System.out.println("Employee #" + (temp + 1) + "\n-----------------");
			System.out.println(employeeArray[temp].toString());
			System.out.print("Earnings: $" + employeeArray[temp].earnings() + "\n-----------------\n");
		}
	}
}

// Declare the abstract Employee super class.
abstract class Employee {
	
	// Initialize the data attributes for the class.
	String firstName = "Default";
	String lastName = "Default";
	String id = "Default";
	String address = "Default";
	String city = "Default";
	String state = "Default";
	
	// Constructor methods.
	// Constructor using default values.
	protected Employee() {
	}
		
	// Constructor using user-defined values.
	protected Employee(String newFirst, String newLast,  String newID, String newAddress, String newCity, String newState) {
		this.firstName = newFirst;
		this.lastName = newLast;
		this.id = newID;
		this.address = newAddress;
		this.city = newCity;
		this.state = newState;
	}
	
	// Defines the toString function, prints data attributes for specified object.
	public String toString() {
		return "First Name: " + firstName + "\nLast Name: " + lastName + "\nEmployee ID: " + id + "\nStreet Address: " + address + "\nCity: " + city + "\nState: " + state;
	}
	
	// Declare abstract earnings method.
	public abstract double earnings();
}

// Declare the HourlyEmployee sub class.
class HourlyEmployee extends Employee {
	
	// Initialize the data attributes for the class.
	double hourlyRate = 0;
	double hoursWorked = 0;
	
	// Constructor methods.
	// Constructor using default values.
	HourlyEmployee() {
	}
		
	// Constructor using user-defined values.
	HourlyEmployee(String newFirst, String newLast, String newID, String newAddress, String newCity, String newState, double newRate, double newHours) {
		super(newFirst, newLast, newID, newAddress, newCity, newState);
		this.hourlyRate = newRate;
		this.hoursWorked = newHours;
	}
	
	// Defines the toString function, prints data attributes for specified object.
	public String toString() {
		return super.toString() + "\nHourly Wage: $" + hourlyRate + "\nHours Worked: " + hoursWorked;
	}
	
	// Implement the abstract earnings method from super class.
	@Override
	public double earnings() {
		double earnings = hourlyRate * hoursWorked;
		
		return earnings;
	}
}
