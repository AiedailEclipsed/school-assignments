function getGradeEntry() {
	if (document.getElementById("undergrad").checked && document.getElementById("webdev").checked) {
		document.getElementById("undergradWebdev").style.display = "block";
		document.getElementById("undergradData").style.display = "none";
		document.getElementById("gradWebdev").style.display = "none";
		document.getElementById("gradData").style.display = "none";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("undergrad").checked && document.getElementById("data").checked) {
		document.getElementById("undergradWebdev").style.display = "none";
		document.getElementById("undergradData").style.display = "block";
		document.getElementById("gradWebdev").style.display = "none";
		document.getElementById("gradData").style.display = "none";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("grad").checked && document.getElementById("webdev").checked) {
		document.getElementById("undergradWebdev").style.display = "none";
		document.getElementById("undergradData").style.display = "none";
		document.getElementById("gradWebdev").style.display = "block";
		document.getElementById("gradData").style.display = "none";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("grad").checked && document.getElementById("data").checked) {
		document.getElementById("undergradWebdev").style.display = "none";
		document.getElementById("undergradData").style.display = "none";
		document.getElementById("gradWebdev").style.display = "none";
		document.getElementById("gradData").style.display = "block";
		document.getElementById("evaluateButton").style.display = "block";
	}
}

function evaluateInput() {
	document.getElementById("evaluateButton").style.display = "none";
	
	var temp, begin, end, GPA = 0;
						
	if (document.getElementById("undergrad").checked && document.getElementById("webdev").checked) {
		begin = 0;
		end = 4;
	}
	else if (document.getElementById("undergrad").checked && document.getElementById("data").checked) {
		begin = 4;
		end = 8;
	}
	else if (document.getElementById("grad").checked && document.getElementById("webdev").checked) {
		begin = 8;
		end = 12;
	}
	else if (document.getElementById("grad").checked && document.getElementById("data").checked) {
		begin = 12;
		end = 16;
	}
	
	for (temp = begin; temp < end; temp++) {
		GPA += parseFloat(document.application.grade[temp].value);
	}
	
	GPA /= 4;
	GPA = GPA.toFixed(1);
	
	if (document.getElementById("undergrad").checked && GPA > 3.2) {
		document.getElementById("goodResults").style.display = "block";
		document.getElementById("badResults").style.display = "none";
	}
	else if (document.getElementById("grad").checked && GPA > 3.7) {
		document.getElementById("goodResults").style.display = "block";
		document.getElementById("badResults").style.display = "none";
	}
	else {
		document.getElementById("goodResults").style.display = "none";
		document.getElementById("badResults").style.display = "block";
	}
	
	document.getElementById("GPA1").innerHTML = GPA;
	document.getElementById("GPA2").innerHTML = GPA;
	
	document.getElementById("resetButton").style.display = "block";
}

function startOver() {
	document.getElementById("undergradWebdev").style.display = "none";
	document.getElementById("undergradData").style.display = "none";
	document.getElementById("gradWebdev").style.display = "none";
	document.getElementById("gradData").style.display = "none";
	document.getElementById("goodResults").style.display = "none";
	document.getElementById("badResults").style.display = "none";
	document.getElementById("resetButton").style.display = "none";
}