function getGradeEntry() {
	if (document.getElementById("undergrad").checked && document.getElementById("webdev").checked) {
		document.getElementById("gradeEntry").style.display = "block";
		document.getElementById("class1").innerHTML = "CSE3153 Database Systems";
		document.getElementById("class2").innerHTML = "CSE3203 Overview of Mobile Systems";
		document.getElementById("class3").innerHTML = "IT4203 Advanced Web Development";
		document.getElementById("class4").innerHTML = "IT4213 Mobile Web Development";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("undergrad").checked && document.getElementById("data").checked) {
		document.getElementById("gradeEntry").style.display = "block";
		document.getElementById("class1").innerHTML = "IT3703 Introduction to Data Analytics & Technology";
		document.getElementById("class2").innerHTML = "IT3883 Advanced Application Development";
		document.getElementById("class3").innerHTML = "IT4153 Advanced Database";
		document.getElementById("class4").innerHTML = "IT4713 Business Intelligence Systems";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("grad").checked && document.getElementById("webdev").checked) {
		document.getElementById("gradeEntry").style.display = "block";
		document.getElementById("class1").innerHTML = "IT5433 Databases: Design & Applications";
		document.getElementById("class2").innerHTML = "IT5443 Web Technologies & Application Development";
		document.getElementById("class3").innerHTML = "IT6203 IT Design Studio";
		document.getElementById("class4").innerHTML = "IT6753 Advanced Web Development";
		document.getElementById("evaluateButton").style.display = "block";
	}
	else if (document.getElementById("grad").checked && document.getElementById("data").checked) {
		document.getElementById("gradeEntry").style.display = "block";
		document.getElementById("class1").innerHTML = "IT6713 Business Intelligence";
		document.getElementById("class2").innerHTML = "IT6733 Database Administration";
		document.getElementById("class3").innerHTML = "IT6773 Practical Data Analytics";
		document.getElementById("class4").innerHTML = "IT7113 Data Visualization";
		document.getElementById("evaluateButton").style.display = "block";
	}
}

function evaluateInput() {
	document.getElementById("evaluateButton").style.display = "none";
	
	var temp, GPA = 0;
						
	for (temp = 0; temp < document.application.grade.length; temp++) {
		GPA += parseFloat(document.application.grade[temp].value);
	}
	
	GPA /= document.application.grade.length;
	GPA = GPA.toFixed(1);
	
	if (document.getElementById("undergrad").checked && GPA > 3.2) {
		document.getElementById("goodResults").style.display = "block";
		document.getElementById("badResults").style.display = "none";
	}
	else if (document.getElementById("grad").checked && GPA > 3.7) {
		document.getElementById("goodResults").style.display = "block";
		document.getElementById("badResults").style.display = "none";
	}
	else {
		document.getElementById("goodResults").style.display = "none";
		document.getElementById("badResults").style.display = "block";
	}
	
	document.getElementById("GPA1").innerHTML = GPA;
	document.getElementById("GPA2").innerHTML = GPA;
	
	document.getElementById("undergrad").disabled = true;
	document.getElementById("grad").disabled = true;
	document.getElementById("webdev").disabled = true;
	document.getElementById("data").disabled = true;
	document.getElementById("grade1").disabled = true;
	document.getElementById("grade2").disabled = true;
	document.getElementById("grade3").disabled = true;
	document.getElementById("grade4").disabled = true;
	
	document.getElementById("resetButton").style.display = "block";
}

function startOver() {
	document.getElementById("gradeEntry").style.display = "none";
	document.getElementById("goodResults").style.display = "none";
	document.getElementById("badResults").style.display = "none";
	document.getElementById("resetButton").style.display = "none";
	document.getElementById("undergrad").disabled = false;
	document.getElementById("grad").disabled = false;
	document.getElementById("webdev").disabled = false;
	document.getElementById("data").disabled = false;
	document.getElementById("grade1").disabled = false;
	document.getElementById("grade2").disabled = false;
	document.getElementById("grade3").disabled = false;
	document.getElementById("grade4").disabled = false;
}